package com.inspector.helpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by alexander.kozlov on 10/18/13.
 */
public class GsonHelper
{
    public static Gson getInstance()
    {
        return new GsonBuilder().setDateFormat("dd.MM.yyyy HH:mm:ss.SSS").create();
    }
}
