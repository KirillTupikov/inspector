package com.inspector.helpers;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.inspector.data.Article;
import com.inspector.data.Car;
import com.inspector.data.ImageCar;
import com.inspector.data.Violation;
import com.inspector.managers.dao.ArticleDAO;
import com.inspector.managers.dao.CarDAO;
import com.inspector.managers.dao.ImageCarDAO;
import com.inspector.managers.dao.ViolationDAO;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * Created by kirill.tupikov on 7/11/13.
 */
public class DataBaseHelper extends OrmLiteSqliteOpenHelper
{
    private static final String TAG = DataBaseHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "inspector.sqlite";

    private static final int DATABASE_VERSION = 1;

    private ArticleDAO articleDAO = null;
    private CarDAO carDAO = null;
    private ViolationDAO violationDAO = null;
    private ImageCarDAO imageCarDAO = null;


    public DataBaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource)
    {
        try
        {
            TableUtils.createTable(connectionSource, Article.class);
            TableUtils.createTable(connectionSource, Car.class);
            TableUtils.createTable(connectionSource, Violation.class);
            TableUtils.createTable(connectionSource, ImageCar.class);
        }
        catch (SQLException e)
        {
            Log.e(TAG, "error creating DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
        catch (java.sql.SQLException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer, int newVer)
    {
        try
        {
            TableUtils.dropTable(connectionSource, Article.class, true);
            TableUtils.dropTable(connectionSource, Car.class, true);
            TableUtils.dropTable(connectionSource, Violation.class, true);
            TableUtils.dropTable(connectionSource, ImageCar.class, true);

            onCreate(db, connectionSource);
        }
        catch (SQLException e)
        {
            Log.e(TAG, "error upgrading db " + DATABASE_NAME + "from ver " + oldVer);
            throw new RuntimeException(e);
        }
        catch (java.sql.SQLException e)
        {
            e.printStackTrace();
        }
    }

    public ArticleDAO getArticleDAO() throws SQLException, java.sql.SQLException
    {
        if (articleDAO == null)
        {
            articleDAO = new ArticleDAO(getConnectionSource(), Article.class);
        }
        return articleDAO;
    }

    public CarDAO getCarDAO() throws SQLException, java.sql.SQLException
    {
        if (carDAO == null)
        {
            carDAO = new CarDAO(getConnectionSource(), Car.class);
        }
        return carDAO;
    }

    public ViolationDAO getViolationDAO() throws SQLException, java.sql.SQLException
    {
        if (violationDAO == null)
        {
            violationDAO = new ViolationDAO(getConnectionSource(), Violation.class);
        }
        return violationDAO;
    }

    public ImageCarDAO getImageCarDAO() throws SQLException, java.sql.SQLException
    {
        if (imageCarDAO == null)
        {
            imageCarDAO = new ImageCarDAO(getConnectionSource(), ImageCar.class);
        }
        return imageCarDAO;
    }


    @Override
    public void close()
    {
        super.close();
        articleDAO = null;
        carDAO = null;
        violationDAO = null;
        imageCarDAO = null;
    }

}
