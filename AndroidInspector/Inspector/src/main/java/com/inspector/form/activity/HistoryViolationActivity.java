package com.inspector.form.activity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.inspector.R;
import com.inspector.data.Violation;
import com.inspector.form.ActivityLauncher;
import com.inspector.gui.HistoryListAdapter;
import com.inspector.managers.DataBaseManager;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 11/11/13.
 */
public class HistoryViolationActivity extends BaseActivity
{
    private ListView historyViolationList;

    private ArrayList<Violation> violations;
    private HistoryListAdapter historyListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_history_violation);

        setUI();
    }

    private void setUI()
    {
        setTitleActionBar(getString(R.string.HistoryViolation_label_history));

        historyViolationList = (ListView) findViewById(R.id.historyViolationList);
        createList();

        registerForContextMenu(historyViolationList);
    }

    private void createList()
    {
        violations = new ArrayList<Violation>();

        historyListAdapter = new HistoryListAdapter(context, violations);
        historyViolationList.setAdapter(historyListAdapter);

        historyViolationList.setClickable(true);
        historyViolationList.setOnItemClickListener(onClickListenerListViewItem);

        updateList();
    }

    private void updateList()
    {
        violations.clear();

        try
        {
            violations.addAll(DataBaseManager.getInstance().getAllViolation());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        historyListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        getMenuInflater().inflate(R.menu.menu_history, menu);
    }


    public AdapterView.OnItemClickListener onClickListenerListViewItem = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
        {
            ActivityLauncher.showNotificationAct(context, violations.get(i).getId());
        }
    };


    @Override
    public boolean onContextItemSelected(MenuItem item)
    {

        int index = violations.get(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position).getId();

        switch (item.getItemId())
        {
            case R.id.history_item_view:
                ActivityLauncher.showNotificationAct(context, index);
                break;

            case R.id.history_item_delete:
                try
                {
                    DataBaseManager.getInstance().deleteViolation(violations.get(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position));
                    updateList();
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onResume()
    {
        updateList();
        super.onResume();
    }
}
