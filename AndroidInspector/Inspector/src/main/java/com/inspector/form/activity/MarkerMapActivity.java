/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspector.form.activity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.inspector.R;

import java.util.ArrayList;

/**
 * This shows how to place markers on a map.
 */
public class MarkerMapActivity extends FragmentActivity
        implements OnMarkerClickListener, OnInfoWindowClickListener, OnMarkerDragListener
{
    private static final LatLng BRISBANE = new LatLng(-27.47093, 153.0235);
    private static final LatLng MELBOURNE = new LatLng(-37.81319, 144.96298);
    private static final LatLng SYDNEY = new LatLng(-33.87365, 151.20689);
    private static final LatLng ADELAIDE = new LatLng(-34.92873, 138.59995);
    private static final LatLng PERTH = new LatLng(-31.952854, 115.857342);

    ArrayList<LatLng> lats = new ArrayList<LatLng>();


    /**
     * Demonstrates customizing the info window and/or its contents.
     */
    class CustomInfoWindowAdapter implements InfoWindowAdapter
    {
        //private final RadioGroup mOptions;

        // These a both viewgroups containing an ImageView with id "badge" and two TextViews with id
        // "title" and "snippet".
        private final View mWindow;
        private final View mContents;

        CustomInfoWindowAdapter()
        {
            mWindow = getLayoutInflater().inflate(R.layout.custom_info_window, null);
            mContents = getLayoutInflater().inflate(R.layout.custom_info_contents, null);
            //mOptions = (RadioGroup) findViewById(R.id.custom_info_window_options);
        }

        @Override
        public View getInfoWindow(Marker marker)
        {
//            if (mOptions.getCheckedRadioButtonId() != R.id.custom_info_window)
//            {
//                // This means that getInfoContents will be called.
//                return null;
//            }
            render(marker, mWindow);
            return mWindow;
        }

        @Override
        public View getInfoContents(Marker marker)
        {
//            if (mOptions.getCheckedRadioButtonId() != R.id.custom_info_contents)
//            {
//                // This means that the default info contents will be used.
//                return null;
//            }
            render(marker, mContents);
            return mContents;
        }

        private void render(Marker marker, View view)
        {
            int badge = 0;
//            // Use the equals() method on a Marker to check for equals.  Do not use ==.
//            if (marker.equals(mBrisbane))
//            {
//                badge = R.drawable.badge_qld;
//            }
//            else
//                if (marker.equals(mAdelaide))
//                {
//                    badge = R.drawable.badge_sa;
//                }
//                else
//                    if (marker.equals(mSydney))
//                    {
//                        badge = R.drawable.badge_nsw;
//                    }
//                    else
//                        if (marker.equals(mMelbourne))
//                        {
//                            badge = R.drawable.badge_victoria;
//                        }
//                        else
//                            if (marker.equals(mPerth))
//                            {
//                                badge = R.drawable.badge_wa;
//                            }
//                            else
//                            {
//                                // Passing 0 to setImageResource will clear the image view.
//                                badge = 0;
//                                ;
//                            }

            ((ImageView) view.findViewById(R.id.badge)).setImageResource(badge);

            String title = marker.getTitle();
            TextView titleUi = ((TextView) view.findViewById(R.id.title));
            if (title != null)
            {
                // Spannable string allows us to edit the formatting of the text.
                SpannableString titleText = new SpannableString(title);
                titleText.setSpan(new ForegroundColorSpan(Color.RED), 0, titleText.length(), 0);
                titleUi.setText(titleText);
            }
            else
            {
                titleUi.setText("");
            }

            String snippet = marker.getSnippet();
            TextView snippetUi = ((TextView) view.findViewById(R.id.snippet));
            if (snippet != null && snippet.length() > 12)
            {
                SpannableString snippetText = new SpannableString(snippet);
                snippetText.setSpan(new ForegroundColorSpan(Color.MAGENTA), 0, 10, 0);
                snippetText.setSpan(new ForegroundColorSpan(Color.BLUE), 12, snippet.length(), 0);
                snippetUi.setText(snippetText);
            }
            else
            {
                snippetUi.setText("");
            }
        }
    }

    void getCurrentLocation()
    {
        Location myLocation = mMap.getMyLocation();
        if (myLocation != null)
        {
            double dLatitude = myLocation.getLatitude();
            double dLongitude = myLocation.getLongitude();
            Log.i("APPLICATION", " : " + dLatitude);
            Log.i("APPLICATION", " : " + dLongitude);
            mMap.addMarker(new MarkerOptions().position(
                    new LatLng(dLatitude, dLongitude)).title("My Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.arrow)));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(dLatitude, dLongitude), 8));

        }
        else
        {
            Toast.makeText(this, "Unable to fetch the current location", Toast.LENGTH_SHORT);
        }

    }

    private GoogleMap mMap;

    private Marker mPerth;
    private Marker mSydney;
    private Marker mBrisbane;
    private Marker mAdelaide;
    private Marker mMelbourne;
    private TextView mTopText;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marker_map);

        //mTopText = (TextView) findViewById(R.id.top_text);

        setUpMapIfNeeded();

        getCurrentLocation();/////////////
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded()
    {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null)
        {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null)
            {
                setUpMap();
            }
        }
    }

    private void setUpMap()
    {
        // Hide the zoom controls as the button panel will cover it.
        mMap.getUiSettings().setZoomControlsEnabled(false);

        // Add lots of markers to the map.
        addMarkersToMap();

        // Setting an info window adapter allows us to change the both the contents and look of the
        // info window.
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());

        // Set listeners for marker events.  See the bottom of this class for their behavior.
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMarkerDragListener(this);

        // Pan to see all markers in view.
        // Cannot zoom to bounds until the map has a size.
        final View mapView = getSupportFragmentManager().findFragmentById(R.id.map).getView();
        if (mapView.getViewTreeObserver().isAlive())
        {
            mapView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener()
            {
                @SuppressWarnings("deprecation") // We use the new method when supported
                @SuppressLint("NewApi") // We check which build version we are using.
                @Override
                public void onGlobalLayout()
                {
                    LatLngBounds bounds = new LatLngBounds.Builder()
                            .include(PERTH)
                            .include(SYDNEY)
                            .include(ADELAIDE)
                            .include(BRISBANE)
                            .include(MELBOURNE)
                            .build();
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                    {
                        mapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                    else
                    {
                        mapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
                }
            });
        }
    }

    private void addMarkersToMap()
    {

        lats.add(new LatLng(52.39124921474462, 31.029620084225975));
        lats.add(new LatLng(52.38995939349948, 31.029845389783212));
        lats.add(new LatLng(52.44603999243712, 30.99407904720778));
        lats.add(new LatLng(52.44694794575534, 30.993574791912994));
        lats.add(new LatLng(52.478594940569806, 30.987273341452404));
        lats.add(new LatLng(52.47864516934081, 30.986788774906707));
        lats.add(new LatLng(52.41638015940266, 30.96247358353315));
        lats.add(new LatLng(52.482253266193545, 31.004421470083418));
        lats.add(new LatLng(52.48294103913787, 31.0032117938177));
        lats.add(new LatLng(52.48319835176516, 31.004060829309257));
        lats.add(new LatLng(52.40874829144371, 30.964246924018973));
        lats.add(new LatLng(52.43380075323283, 31.00114422640842));
        lats.add(new LatLng(52.43374173553156, 31.002351220465123));
        lats.add(new LatLng(52.433174505796714, 31.005162175512737));
        lats.add(new LatLng(52.43341713675472, 31.004250224447688));
        lats.add(new LatLng(52.45957622505754, 30.99482173193938));
        lats.add(new LatLng(52.459569671394256, 30.996026558057444));
        lats.add(new LatLng(52.420086083663826, 30.972545488692372));
        lats.add(new LatLng(52.420550170693645, 30.97295050225362));
        lats.add(new LatLng(52.43162602555516, 30.992239958133908));
        lats.add(new LatLng(52.43071775487206, 30.992728120174643));
        lats.add(new LatLng(52.43363435158616, 30.99259216895015));
        lats.add(new LatLng(52.43198226294521, 30.996212294014917));
        lats.add(new LatLng(52.43049911614212, 30.99432853843414));
        lats.add(new LatLng(52.43217966010344, 30.993579204750457));
        lats.add(new LatLng(52.46584708789798, 31.030612607870747));
        lats.add(new LatLng(52.46538839476876, 31.031535287771874));
        lats.add(new LatLng(52.4654915039816, 30.983853318295));
        lats.add(new LatLng(52.465127823299774, 30.984550692638866));
        lats.add(new LatLng(52.447470538818564, 30.99838632918793));
        lats.add(new LatLng(52.40554689800021, 30.920322777324955));
        lats.add(new LatLng(52.45387719221631, 31.00523326866266));
        lats.add(new LatLng(52.4537034972169, 31.004981141015268));
        lats.add(new LatLng(52.454086298286725, 31.024181803450425));
        lats.add(new LatLng(52.45523331797575, 31.025034745917154));
        lats.add(new LatLng(52.399944431969665, 31.023066827637873));
        lats.add(new LatLng(52.35819198457561, 31.030614850792286));
        lats.add(new LatLng(52.358685861015886, 31.029840339266265));
        lats.add(new LatLng(52.35779785366733, 31.030539748939866));
        lats.add(new LatLng(52.47174234501682, 30.979306263522577));
        lats.add(new LatLng(52.47131444352527, 30.9792300129749));
        lats.add(new LatLng(52.42359659470324, 31.007644662578777));
        lats.add(new LatLng(52.45918639282798, 31.02437141955395));
        lats.add(new LatLng(52.46025135956345, 31.024682555799686));
        lats.add(new LatLng(52.470378592781884, 31.03555961956068));
        lats.add(new LatLng(52.468177536145724, 31.039673473024187));
        lats.add(new LatLng(52.46954696132529, 31.042913581514167));
        lats.add(new LatLng(52.46878362673439, 31.041626121187015));
        lats.add(new LatLng(52.4706903002655, 31.042951132440376));
        lats.add(new LatLng(52.470613370080116, 31.04288068109705));
        lats.add(new LatLng(52.424622225168044, 31.01424666701799));
        lats.add(new LatLng(52.425210881635486, 31.015029872050352));
        lats.add(new LatLng(52.425655238248986, 31.013742411723204));
        lats.add(new LatLng(52.42554865865981, 31.01389261542802));
        lats.add(new LatLng(52.44442849559822, 31.01933681681265));
        lats.add(new LatLng(52.444182647588214, 31.0205330820333));
        lats.add(new LatLng(52.45168378119426, 30.994674375424953));
        lats.add(new LatLng(52.45075298354979, 30.995908191571814));
        lats.add(new LatLng(52.45557163004871, 30.991633914821136));
        lats.add(new LatLng(52.455447098896975, 30.989938758723728));
        lats.add(new LatLng(52.425744803900564, 30.94619382503771));
        lats.add(new LatLng(52.42678762839581, 30.946832190783258));
        lats.add(new LatLng(52.412687995190396, 30.951831502246197));
        lats.add(new LatLng(52.46106267634826, 31.000987089635526));
        lats.add(new LatLng(52.461108094614694, 31.000221848906904));
        lats.add(new LatLng(52.46113378822882, 31.039198448549186));
        lats.add(new LatLng(52.46113378822882, 31.039198448549186));
        lats.add(new LatLng(52.452511923547306, 30.96332788807263));
        lats.add(new LatLng(52.4508469931656, 30.96600473266948));
        lats.add(new LatLng(52.408691614504, 30.93626086693901));
        lats.add(new LatLng(52.40518982525354, 30.941191831249988));
        lats.add(new LatLng(52.37689636953685, 31.02978652101727));
        lats.add(new LatLng(52.37579504692422, 31.02945411560299));
        lats.add(new LatLng(52.450660890109894, 31.02322318406547));
        lats.add(new LatLng(52.4519456505671, 31.024178050474777));
        lats.add(new LatLng(52.43114570326311, 31.010623826470926));
        lats.add(new LatLng(52.472891528890045, 31.009909367396762));
        lats.add(new LatLng(52.47200704440639, 31.00915834887261));
        lats.add(new LatLng(52.471620486318365, 31.01126120074028));
        lats.add(new LatLng(52.46791028782503, 31.007646394068036));
        lats.add(new LatLng(52.46831653573347, 31.007560563379563));
        lats.add(new LatLng(52.47098137693198, 31.033564056053613));
        lats.add(new LatLng(52.47196415938484, 31.032185400619962));
        lats.add(new LatLng(52.457089311790895, 30.992336113008754));
        lats.add(new LatLng(52.395036822075284, 31.02825940032043));
        lats.add(new LatLng(52.39454457081787, 31.02888167281187));
        lats.add(new LatLng(52.4404140708109, 30.99861232441346));
        lats.add(new LatLng(52.44028621831014, 30.99891273182312));
        lats.add(new LatLng(52.46789219338088, 31.02090799286549));
        lats.add(new LatLng(52.469248523052926, 31.0213264174718));
        lats.add(new LatLng(52.46907816511088, 31.020124787833126));
        lats.add(new LatLng(52.46819032741775, 31.022098893668087));
        lats.add(new LatLng(52.477509181738924, 31.029056211911616));
        lats.add(new LatLng(52.46843111637682, 52.46843111637682));
        lats.add(new LatLng(52.467315014539075, 31.02579391731961));
        lats.add(new LatLng(52.435284272896, 31.006418709100533));
        lats.add(new LatLng(52.438147101463535, 30.996326193684794));
        lats.add(new LatLng(52.43827168177799, 30.99693773734021));
        lats.add(new LatLng(52.43770049788245, 31.001966972516087));
        lats.add(new LatLng(52.43835613372653, 31.00360286833435));
        lats.add(new LatLng(52.47277942805671, 31.02982108097169));
        lats.add(new LatLng(52.47363769071095, 31.028447789956065));
        lats.add(new LatLng(52.454287851025235, 30.99315031544255));
        lats.add(new LatLng(52.449024563607644, 30.991570411816927));
        lats.add(new LatLng(52.45017817935362, 30.991014853896793));
        lats.add(new LatLng(52.442217405245664, 31.01079959043813));
        lats.add(new LatLng(52.442158398872806, 31.00927073129964));
        lats.add(new LatLng(52.468177536145724, 31.039673473024187));
        lats.add(new LatLng(52.46228533813201, 30.986402682912573));
        lats.add(new LatLng(52.46154645211531, 30.987421922338225));
        lats.add(new LatLng(52.439510295447775, 31.004759640362213));
        lats.add(new LatLng(52.439982372721616, 31.00375649419064));
        lats.add(new LatLng(52.44708728274978, 31.022188829608815));
        lats.add(new LatLng(52.448580112401125, 31.023240994457918));
        lats.add(new LatLng(52.47514260944597, 31.023397303884547));
        lats.add(new LatLng(52.47597789655603, 31.021981097524687));
        lats.add(new LatLng(52.47504106365213, 31.022399522130975));
        lats.add(new LatLng(52.406144690291384, 30.982497165284148));
        lats.add(new LatLng(52.40599705285191, 30.98125530251025));
        lats.add(new LatLng(52.45055876042654, 30.972470488442873));
        lats.add(new LatLng(52.445754592646395, 30.999368372918816));
        lats.add(new LatLng(52.444430315139144, 31.00077921486065));
        lats.add(new LatLng(52.43498459388421, 31.00535706873707));
        lats.add(new LatLng(52.43612610996429, 31.007478696067835));
        lats.add(new LatLng(52.465844108423845, 31.029354482538636));
        lats.add(new LatLng(52.434674902587986, 31.00818235015667));
        lats.add(new LatLng(52.432894034474224, 31.009830295445383));
        lats.add(new LatLng(52.47043966846015, 31.028569152942286));
        lats.add(new LatLng(52.47997366882573, 31.023512473716213));

        ArrayList<Marker> markers = new ArrayList<Marker>();
        for (LatLng lat : lats)
        {
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(lat)
                    .title("Brisbane")
                    .snippet("Population: 37")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            markers.add(marker);
        }

        // Uses a colored icon.
        mBrisbane = mMap.addMarker(new MarkerOptions()
                .position(BRISBANE)
                .title("Brisbane")
                .snippet("Population: 2,074,200")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

        // Uses a custom icon.
        mSydney = mMap.addMarker(new MarkerOptions()
                .position(SYDNEY)
                .title("Sydney")
                .snippet("Population: 4,627,300")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.arrow)));

        // Creates a draggable marker. Long press to drag.
        mMelbourne = mMap.addMarker(new MarkerOptions()
                .position(MELBOURNE)
                .title("Melbourne")
                .snippet("Population: 4,137,400")
                .draggable(true));

        // A few more markers for good measure.
        mPerth = mMap.addMarker(new MarkerOptions()
                .position(PERTH)
                .title("Perth")
                .snippet("Population: 1,738,800"));
        mAdelaide = mMap.addMarker(new MarkerOptions()
                .position(ADELAIDE)
                .title("Adelaide")
                .snippet("Population: 1,213,000"));

        // Creates a marker rainbow demonstrating how to create default marker icons of different
        // hues (colors).
        int numMarkersInRainbow = 12;
        for (int i = 0; i < numMarkersInRainbow; i++)
        {
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(
                            -30 + 10 * Math.sin(i * Math.PI / (numMarkersInRainbow - 1)),
                            135 - 10 * Math.cos(i * Math.PI / (numMarkersInRainbow - 1))))
                    .title("Marker " + i)
                    .icon(BitmapDescriptorFactory.defaultMarker(i * 360 / numMarkersInRainbow)));
        }
    }

    private boolean checkReady()
    {
        if (mMap == null)
        {
            // Toast.makeText(this, R.string.map_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    /**
     * Called when the Clear button is clicked.
     */
    public void onClearMap(View view)
    {
        if (!checkReady())
        {
            return;
        }
        mMap.clear();
    }

    /**
     * Called when the Reset button is clicked.
     */
    public void onResetMap(View view)
    {
        if (!checkReady())
        {
            return;
        }
        // Clear the map because we don't want duplicates of the markers.
        mMap.clear();
        addMarkersToMap();
    }

    //
    // Marker related listeners.
    //

    @Override
    public boolean onMarkerClick(final Marker marker)
    {
        // This causes the marker at Perth to bounce into position when it is clicked.
        if (marker.equals(mPerth))
        {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            Projection proj = mMap.getProjection();
            Point startPoint = proj.toScreenLocation(PERTH);
            startPoint.offset(0, -100);
            final LatLng startLatLng = proj.fromScreenLocation(startPoint);
            final long duration = 1500;

            final Interpolator interpolator = new BounceInterpolator();

            handler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);
                    double lng = t * PERTH.longitude + (1 - t) * startLatLng.longitude;
                    double lat = t * PERTH.latitude + (1 - t) * startLatLng.latitude;
                    marker.setPosition(new LatLng(lat, lng));

                    if (t < 1.0)
                    {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    }
                }
            });
        }
        // We return false to indicate that we have not consumed the event and that we wish
        // for the default behavior to occur (which is for the camera to move such that the
        // marker is centered and for the marker's info window to open, if it has one).
        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker)
    {
        Toast.makeText(getBaseContext(), "Click Info Window", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMarkerDragStart(Marker marker)
    {
        mTopText.setText("onMarkerDragStart");
    }

    @Override
    public void onMarkerDragEnd(Marker marker)
    {
        mTopText.setText("onMarkerDragEnd");
    }

    @Override
    public void onMarkerDrag(Marker marker)
    {
        mTopText.setText("onMarkerDrag.  Current Position: " + marker.getPosition());
    }
}
