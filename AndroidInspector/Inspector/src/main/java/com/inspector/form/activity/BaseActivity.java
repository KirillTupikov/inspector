package com.inspector.form.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

import com.inspector.R;
import com.inspector.exception.MobileServerAPIException;
import com.inspector.form.ActivityLauncher;
import com.inspector.interfaces.IDialogSupportedActivity;
import com.inspector.form.dialog.AppModalDialog;
import com.inspector.form.dialog.ButtonDialog;
import com.inspector.managers.SessionManager;
import com.inspector.net.MobileServerApi;
import com.inspector.net.data.request.LogoutRequest;
import com.inspector.utils.NfcStatus;
import com.inspector.utils.NfcUtil;
import com.inspector.utils.Utils;

public class BaseActivity extends Activity implements IDialogSupportedActivity
{
    protected ActionBar actionBar;
    protected Context context;
    protected Handler handler;

    protected MobileServerApi mobileServerApi;

    private AppModalDialog modalDialog;

    protected final String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        context = this;

        handler = new Handler();

        mobileServerApi = new MobileServerApi();

        actionBar = getActionBar();
        if (actionBar != null)
        {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#1d4c7c")));
            actionBar.setIcon(R.color.blue);
        }
    }

    protected void setIconActionBar(int idDrawable)
    {
        actionBar.setIcon(idDrawable);
    }

    protected void enabledButtonHome()
    {
        actionBar.setDisplayHomeAsUpEnabled(false);
    }

    protected void setTitleActionBar(String titleActionBar)
    {
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(titleActionBar);
    }

    protected boolean checkNetworkStatus(final boolean closeView)
    {
        try
        {
            final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = connMgr.getActiveNetworkInfo();

            final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (nInfo != null)
            {
                if ((wifi.isAvailable() || mobile.isAvailable()) && nInfo.isConnected())
                {
                    //if(connMgr.requestRouteToHost(ConnectivityManager.TYPE_WIFI, ) )

                    if (Utils.isOnline(context))
                        return true;
                    else
                        return false;
                }
            }

            showDialog(WARNING_DIALOG, getString(R.string.Dialogs_connection), getString(R.string.Dialogs_problem_with_ethernet),
                    new ButtonDialog(
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    if (closeView)
                                        finish();
                                }
                            }, getString(R.string.Dialogs_continue))
                    , null);

            return false;
        }
        catch (NullPointerException ex)
        {
            return false;
        }
    }

//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu)
//    {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        //getMenuInflater().inflate(R.menu.base, menu);
//        getMenuInflater().inflate(R.menu.menu_button, menu);
//        return true;
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        MenuItem mi = menu.add(0, 1, 0, getString(R.string.menu_setting));
        mi.setIntent(new Intent(this, PreferenceApplication.class));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId())
        {
            case R.id.menuLogout:

                showDialog(WARNING_DIALOG,
                        getString(R.string.Dialogs_exit_act_title_logout),
                        getString(R.string.Dialogs_exit_act_description_logout),
                        new ButtonDialog(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                if (!SessionManager.getInstance().getToken().isEmpty())
                                {
                                    Thread processMobileServerApiSendViolationThread = new Thread()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            try
                                            {
                                                showDialog(WAITING_DIALOG,
                                                        getString(R.string.Dialogs_exit_act_title_logout),
                                                        getString(R.string.Dialogs_exit_act_description_end_session), null, null);

                                                mobileServerApi.logout(new LogoutRequest(SessionManager.getInstance().getToken()));

                                                SessionManager.getInstance().close(context);
                                                ActivityLauncher.ExitAct(context);
                                                finish();
                                            }
                                            catch (MobileServerAPIException e)
                                            {
                                                e.printStackTrace();
                                                stopDialog();
                                            }
                                            catch (Exception e)
                                            {
                                                e.printStackTrace();
                                                stopDialog();
                                            }
                                        }
                                    };
                                    processMobileServerApiSendViolationThread.start();
                                }
                            }
                        }, getString(R.string.Dialogs_continue)),
                        new ButtonDialog(new Runnable()
                        {
                            @Override
                            public void run()
                            {

                            }
                        }, getString(R.string.Dialogs_cancel)));

                return true;

            case android.R.id.home:

                finish();

                return true;

            case R.id.menuAboutProgram:
                ActivityLauncher.showAboutProgram(context);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void checkNFCStatus()
    {
        if (NfcUtil.getNfcStatus(context) == NfcStatus.OF)
        {
            showDialog(WARNING_DIALOG,
                    getString(R.string.Dialogs_nfc_title),
                    getString(R.string.Dialogs_nfc_description),
                    new ButtonDialog(
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            }, getString(R.string.Dialogs_setting)),
                    new ButtonDialog(
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {

                                }
                            }, getString(R.string.Dialogs_cancel))
            );
        }
    }


    @Override
    public void showDialog(String dialogType, String title, String message, ButtonDialog actionPositive, ButtonDialog actionNegative)
    {
        if (modalDialog != null)
        {
            modalDialog.stop();
        }

        if (actionPositive != null && actionNegative != null)
        {
            modalDialog = new AppModalDialog(handler, this, dialogType, title, message, actionPositive, actionNegative);
        }
        else
            if (actionPositive != null)
            {
                modalDialog = new AppModalDialog(handler, this, dialogType, title, message, actionPositive);
            }
            else
                if (actionNegative != null)
                {
                    modalDialog = new AppModalDialog(handler, this, dialogType, title, message, null, actionNegative);
                }
                else
                    if (actionPositive == null)
                    {
                        modalDialog = new AppModalDialog(handler, this, dialogType, title, message);
                    }
        runOnUiThread(modalDialog);
    }

    public void stopDialog()
    {
        if (modalDialog != null)
        {
            modalDialog.dismissDialog();
        }
    }
}
