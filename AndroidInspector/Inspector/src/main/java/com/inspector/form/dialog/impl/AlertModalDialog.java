package com.inspector.form.dialog.impl;


import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;

import com.inspector.form.dialog.ButtonDialog;


/**
 * Created with IntelliJ IDEA.
 * User: Kirill
 * Date: 26.04.13
 * Time: 18:36
 * To change this template use File | Settings | File Templates.
 */
public abstract class AlertModalDialog extends AlertDialog implements IModalDialog
{

    protected ButtonDialog actionPositive;
    protected ButtonDialog actionNegative;

    public AlertModalDialog(Context context, String title, String message, ButtonDialog actionPositive, ButtonDialog actionNegative)
    {
//        super(context, R.style.DialogStyle);
        super(context);
        this.setMessage(message);
        this.setCancelable(false);
        this.setTitle(title);
        Drawable icon = getDialogIcon();
        if (icon != null)
        {
            this.setIcon(icon);
        }
        this.actionPositive = actionPositive;
        this.actionNegative = actionNegative;
    }

    @Override
    public void onBackPressed()
    {
        this.cancel();
    }

    protected abstract Drawable getDialogIcon();

    public void close()
    {
        super.cancel();
    }

    protected void runUserActionPositive()
    {
        if (actionPositive != null)
        {
            actionPositive.getAction().run();
        }
    }

    protected void runUserActionNegative()
    {
        if (actionNegative != null)
        {
            actionNegative.getAction().run();
        }
    }

}

