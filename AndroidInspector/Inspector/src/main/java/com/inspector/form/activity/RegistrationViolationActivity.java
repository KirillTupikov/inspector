package com.inspector.form.activity;

import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.inspector.R;
import com.inspector.Setting;
import com.inspector.data.Article;
import com.inspector.data.Violation;
import com.inspector.exception.MobileServerAPIException;
import com.inspector.form.ActivityLauncher;
import com.inspector.form.dialog.ButtonDialog;
import com.inspector.gui.ArticleListAdapter;
import com.inspector.interfaces.IGPSCallback;
import com.inspector.managers.DataBaseManager;
import com.inspector.managers.GPSManager;
import com.inspector.managers.SessionManager;
import com.inspector.net.data.request.ListArticleRequest;
import com.inspector.net.data.response.ListArticleResponse;
import com.inspector.services.GPSTracker;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by kirill.tupikov on 11/11/13.
 */
public class RegistrationViolationActivity extends BaseActivity implements IGPSCallback
{
    private Button buttonNext;
    private Button buttonMap;
    private Button buttonGPS;

    private EditText addressViolation;

    private GPSManager gpsManager = null;
    private String address;

    private ListView violationListView;
    private PullToRefreshListView mPullRefreshListView;
    private ImageView listUsersRefreshIcon;
    private ArrayList<Article> articles;
    private ArticleListAdapter articleListAdapter;

    private Violation violation;
    private GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registration_violation);

        setUI();

        Date date = Calendar.getInstance().getTime();
        violation = new Violation(date, SessionManager.getInstance().getFio(), addressViolation.getText().toString(), articles.get(0));
    }

    private void setUI()
    {
        setTitleActionBar(getString(R.string.RegistrationViolation_label_registration_violation));

        buttonNext = (Button) findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(onClickButtonNext);

        buttonMap = (Button) findViewById(R.id.buttonMap);
        buttonMap.setOnClickListener(onClickButtonMap);

        buttonGPS = (Button) findViewById(R.id.buttonGPS);
        buttonGPS.setOnClickListener(onClickButtonGPS);

        addressViolation = (EditText) findViewById(R.id.addressViolation);

        listUsersRefreshIcon = (ImageView) findViewById(R.id.listUsersRefreshIcon);

        mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.violationList);

        // Set a listener to be invoked when the list should be refreshed.
        mPullRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
        {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView)
            {
                String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
                        DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

                // Update the LastUpdatedLabel
                refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
                // Do work to refresh the list here.

                new GetDataTask().execute();
            }
        });

        createList();

        violationListView.setClickable(true);
        violationListView.setOnItemClickListener(onClickListenerListViewItem);

        if (articles.isEmpty())
        {
            listUsersRefreshIcon.setVisibility(View.VISIBLE);
        }
        else
        {
            listUsersRefreshIcon.setVisibility(View.GONE);
        }
    }

    private class GetDataTask extends AsyncTask<Void, Void, String[]>
    {
        @Override
        protected String[] doInBackground(Void... params)
        {
            try
            {
                ListArticleResponse listArticleResponse = mobileServerApi.getListArticlesViolation(new ListArticleRequest(SessionManager.getInstance().getToken()));

                //пишем в базу полученные статьи, если такая статья есть, то заменяем её
                DataBaseManager.getInstance().updateListArticles(listArticleResponse.getData());

                stopDialog();
            }
            catch (MobileServerAPIException e)
            {
                if (e.getErrorCode() == MobileServerAPIException.SESSION_EXPIRED)//сессия окончена, необходимо обновиться
                {
                    showDialog(ERROR_DIALOG,
                            getString(R.string.Dialog_Exception_title_session_expired),
                            getString(R.string.Dialog_Exception_description_session_expired),
                            new ButtonDialog(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    ActivityLauncher.showInspectorLoginAct(context);
                                }
                            }, getString(R.string.Dialogs_continue)),
                            new ButtonDialog(new Runnable()
                            {
                                @Override
                                public void run()
                                {

                                }
                            }, getString(R.string.Dialogs_cancel)));
                }
                else
                {
                    e.printStackTrace();
                    stopDialog();
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                stopDialog();
            }
            return new String[0];
        }

        @Override
        protected void onPostExecute(String[] strings)
        {
            handler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    createList();
                    updateList();

                    mPullRefreshListView.onRefreshComplete();

                    if (articles.isEmpty())
                    {
                        listUsersRefreshIcon.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        listUsersRefreshIcon.setVisibility(View.GONE);
                    }

                }
            });
            super.onPostExecute(strings);
        }
    }


    private void createList()
    {
        articles = new ArrayList<Article>();

        violationListView = mPullRefreshListView.getRefreshableView();
        articleListAdapter = new ArticleListAdapter(context, articles);
        violationListView.setAdapter(articleListAdapter);

        updateList();
    }

    private void updateList()
    {
        articles.clear();

//        loadArticles();//грузим с сервера

        try
        {
            articles.addAll(DataBaseManager.getInstance().getAllArticles());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        articleListAdapter.notifyDataSetChanged();
    }

    public AdapterView.OnItemClickListener onClickListenerListViewItem = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
        {
            articleListAdapter.setCheckedItemId(articles.get(i - 1).getId());
            articleListAdapter.notifyDataSetChanged();
        }
    };

    private View.OnClickListener onClickButtonNext = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if (addressViolation.getText().toString().isEmpty())
            {
                showDialog(WARNING_DIALOG,
                        getString(R.string.RegistrationViolation_dialog_title_address_is_empty),
                        getString(R.string.RegistrationViolation_dialog_description_address_is_empty), null, null);
            }
            else
            {
                Article article = null;
                for (Article articleBuf : articles)
                {
                    if (articleBuf.getId() == articleListAdapter.getCheckedItemId())
                    {
                        article = articleBuf;
                    }
                }

                //violation = new Violation(date, SessionManager.getInstance().getFio(), addressViolation.getText().toString(), article);
                violation.setAddress(addressViolation.getText().toString());//если адрес введен вручную
                violation.setArticle(article);
                ActivityLauncher.showSnapshotCarAct(context, violation);
            }
        }
    };

    private View.OnClickListener onClickButtonMap = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            ActivityLauncher.showOpenStreetMapActForResult(RegistrationViolationActivity.this, Setting.OPEN_STREET_MAP_VIEW_COD_REQUEST);
        }
    };

    private Runnable onGetGPSLocationRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            // create class object
            gps = new GPSTracker(RegistrationViolationActivity.this);

            // check if GPS enabled
            if (gps.canGetLocation())
            {

                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();
                gps.stopUsingGPS();
                try
                {
                    address = GPSManager.getAddress(context, latitude, longitude);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                if (address != null)
                {
                    addressViolation.setText(address);

                    violation.setAddress(address);
                    violation.setLatitude(String.valueOf(latitude));
                    violation.setLongitude(String.valueOf(longitude));
                }
            }
            else
            {
                gps.showSettingsAlert();
            }
        }
    };


    private class checkGPSLocation extends AsyncTask<String, Void, String>
    {

        protected String doInBackground(String... params)
        {
            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            handler.post(onGetGPSLocationRunnable);

            return null;
        }

        @Override
        protected void onPreExecute()
        {
            showDialog(WAITING_DIALOG,
                    getString(R.string.RegistrationViolation_dialog_label_GPS),
                    getString(R.string.RegistrationViolation_dialog_description_GPS),
                    null,
                    new ButtonDialog(new Runnable()
                    {
                        @Override
                        public void run()
                        {

                        }
                    }, getString(R.string.Dialogs_cancel)));
        }

        @Override
        protected void onPostExecute(String result)
        {
            if (!isFinishing())
            {
                stopDialog();
            }
        }
    }


    private View.OnClickListener onClickButtonGPS = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {

//            checkGPSLocation checkGPSLocation = new checkGPSLocation();
//            checkGPSLocation.execute();

            gpsManager = new GPSManager();
            gpsManager.startListening(context);
            gpsManager.setGPSCallback(RegistrationViolationActivity.this);

            showDialog(WAITING_DIALOG,
                    getString(R.string.RegistrationViolation_dialog_label_GPS),
                    getString(R.string.RegistrationViolation_dialog_description_GPS),
                    null,
                    new ButtonDialog(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            gpsManager.stopListening();
                            gpsManager.setGPSCallback(null);
                            gpsManager = null;
                            // stop();
                        }
                    }, getString(R.string.Dialogs_cancel)));
        }
    };


    @Override
    public void onGPSUpdate(Location location)
    {
        try
        {
            address = gpsManager.getAddress(context, location);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        if (address != null)
        {
            addressViolation.setText(address);

            violation.setAddress(address);
            violation.setLatitude(String.valueOf(location.getLatitude()));
            violation.setLongitude(String.valueOf(location.getLongitude()));
        }

        gpsManager.stopListening();
        gpsManager.setGPSCallback(null);

        gpsManager = null;

        stopDialog();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == Setting.OPEN_STREET_MAP_VIEW_COD_REQUEST)
        {
            if (resultCode == RESULT_OK)
            {
                address = intent.getStringExtra(Violation.ADDRESS_VIOLATION_FIELD_NAME);
                addressViolation.setText(address);

                violation.setAddress(address);
                violation.setLatitude(intent.getStringExtra(Violation.LATITUDE_FIELD_NAME));
                violation.setLongitude(intent.getStringExtra(Violation.LONGITUDE_FIELD_NAME));
            }
        }
    }

}
