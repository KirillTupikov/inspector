package com.inspector.form.dialog.impl;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.inspector.form.dialog.ButtonDialog;


/**
 * Created with IntelliJ IDEA.
 * User: Kirill
 * Date: 26.04.13
 * Time: 18:38
 * To change this template use File | Settings | File Templates.
 */
public class ProgressModalDialog extends ProgressDialog implements IModalDialog
{

    protected ButtonDialog actionPositive;
    protected ButtonDialog actionNegative;

    public ProgressModalDialog(Context context, String title, String message, final ButtonDialog actionPositive, final ButtonDialog actionNegative)
    {
        //super(context, R.style.DialogStyle);
        super(context);
        this.setMessage(message);
        this.setIndeterminate(true);
        this.setCancelable(false);
        this.setTitle(title);
        this.actionPositive = actionPositive;
        this.actionNegative = actionNegative;

        if (actionNegative != null)
            this.setButton(InfoModalDialog.BUTTON_NEGATIVE, actionNegative.getTitle(), new OnClickListener()
            {
                public void onClick(DialogInterface dialog, int which)
                {
                    actionNegative.getAction().run();
                }

            });
    }

    @Override
    public void onBackPressed()
    {
        this.cancel();
    }


    public void close()
    {
        super.cancel();
    }

}
