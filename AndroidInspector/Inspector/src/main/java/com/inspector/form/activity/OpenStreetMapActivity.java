package com.inspector.form.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.inspector.R;
import com.inspector.data.Violation;
import com.inspector.managers.GPSManager;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.MyLocationOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OpenStreetMapActivity extends BaseActivity
{

    private MapView myOpenMapView;
    private MapController myMapController;
    private MyLocationOverlay myLocationOverlay = null;

    private LocationManager locationManager;

    private TextView addressLabel;

    private String address;
    private Location currentLocation;

    private ArrayList<OverlayItem> overlayItemArray;

    private ProgressBar checkGPSLocationProgress;

    private MapOverlay clickOverlay;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_street_map);

        setUI();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Location lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (lastLocation != null)
        {
            updateLoc(lastLocation.getLatitude(), lastLocation.getLongitude(), true);
        }
    }

    private void setUI()
    {
        setTitle(R.string.OpenStreetMap_label);

        checkGPSLocationProgress = (ProgressBar) findViewById(R.id.checkGPSLocationProgress);

        addressLabel = (TextView) findViewById(R.id.addressLabel);

        myOpenMapView = (MapView) findViewById(R.id.openMapView);
        myOpenMapView.setBuiltInZoomControls(true);

        myMapController = myOpenMapView.getController();
        myMapController.setZoom(17);
        myOpenMapView.setMultiTouchControls(true);

        address = "";

        //--- Create Overlay
        overlayItemArray = new ArrayList<OverlayItem>();
//
//        DefaultResourceProxyImpl defaultResourceProxyImpl = new DefaultResourceProxyImpl(this);
//        MyItemizedIconOverlay myItemizedIconOverlay = new MyItemizedIconOverlay(context, overlayItemArray, overlayItemArray, myOnItemGestureListener, defaultResourceProxyImpl);
//        myOpenMapView.getOverlays().add(myItemizedIconOverlay);
//
//        myOpenMapView.invalidate();


        //Add Scale Bar
//        ScaleBarOverlay myScaleBarOverlay = new ScaleBarOverlay(this);
//        myOpenMapView.getOverlays().add(myScaleBarOverlay);

        //Add MyLocationOverlay
        myLocationOverlay = new MyLocationOverlay(context, myOpenMapView);
        myOpenMapView.getOverlays().add(myLocationOverlay);

        clickOverlay = new MapOverlay(context, overlayItemArray, null);
        myOpenMapView.getOverlays().add(clickOverlay);

        myOpenMapView.invalidate();

//        myOpenMapView.setOnTouchListener(new View.OnTouchListener()
//        {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent)
//            {
//                int actionType = motionEvent.getAction();
//                switch (actionType)
//                {
//                    case MotionEvent.ACTION_DOWN:
//                        MapView.Projection proj = myOpenMapView.getProjection();
//                        IGeoPoint loc = proj.fromPixels(motionEvent.getX(), motionEvent.getY());
//                        double longitude = ((double) loc.getLongitudeE6()) / 1000000;
//                        double latitude = ((double) loc.getLatitudeE6()) / 1000000;
//
//                        updateLoc(latitude, longitude, false);
//                }
//                return false;
//            }
//        });

        //myLocationOverlay.enableCompass();
    }

    private synchronized void updateLoc(final double latitude, final double longitude, boolean center)
    {
        currentLocation = new Location("Current location");
        currentLocation.setLatitude(latitude);
        currentLocation.setLongitude(longitude);

        GetGPSAddress getGPSAddress = new GetGPSAddress();
        getGPSAddress.execute();

        GeoPoint locGeoPoint = new GeoPoint(latitude, longitude);

        if (center)
        {
            myMapController.setCenter(locGeoPoint);

            setOverlayLoc(locGeoPoint, false);
        }
        else
            setOverlayLoc(locGeoPoint, true);

        myOpenMapView.invalidate();
    }

    private void setOverlayLoc(GeoPoint locGeoPoint, boolean marker)
    {
        overlayItemArray.clear();

        OverlayItem newMyLocationItem = new OverlayItem("My Location", "My Location", locGeoPoint);
        overlayItemArray.add(newMyLocationItem);

        if (marker)
        {
            myOpenMapView.getOverlays().remove(clickOverlay);
            clickOverlay = new MapOverlay(context, overlayItemArray, null);
            myOpenMapView.getOverlays().add(clickOverlay);
        }
    }

    private class MapOverlay extends ItemizedIconOverlay
    {
        private boolean synch = false;

        public MapOverlay(Context pContext, List pList, OnItemGestureListener pOnItemGestureListener)
        {
            super(pContext, pList, pOnItemGestureListener);
        }

        @Override
        public boolean onTouchEvent(MotionEvent motionEvent, MapView mapView)
        {
            int actionType = motionEvent.getAction();
            switch (actionType)
            {
                case MotionEvent.ACTION_DOWN:
                    synch = true;

                    break;

                case MotionEvent.ACTION_UP:
                    if (synch)
                    {
                        MapView.Projection proj = myOpenMapView.getProjection();
                        IGeoPoint loc = proj.fromPixels(motionEvent.getX(), motionEvent.getY());
                        double longitude = ((double) loc.getLongitudeE6()) / 1000000;
                        double latitude = ((double) loc.getLatitudeE6()) / 1000000;

                        updateLoc(latitude, longitude, false);
                    }

                    break;

                case MotionEvent.ACTION_MOVE:
                    synch = false;

                    break;
            }
            return false;
        }
    }

    private class GetGPSAddress extends AsyncTask<String, Void, String>
    {
        protected String doInBackground(String... params)
        {
            try
            {
                Thread.sleep(500);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            // Update the progress bar
            handler.post(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        String addressAll = GPSManager.getAddress(context, currentLocation.getLatitude(), currentLocation.getLongitude());
                        address = addressAll.replaceAll(", null", "");
                        addressLabel.setText(address);
                       // checkGPSLocationProgress.setVisibility(View.INVISIBLE);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }

        @Override
        protected void onPreExecute()
        {
            checkGPSLocationProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result)
        {
            checkGPSLocationProgress.setVisibility(View.INVISIBLE);
//            if (!isFinishing())
//            {
//                checkGPSLocationProgress.setVisibility(View.INVISIBLE);
//            }
        }
    }

    private LocationListener myLocationListener = new LocationListener()
    {
        @Override
        public void onLocationChanged(Location location)
        {
            // TODO Auto-generated method stub
            updateLoc(location.getLatitude(), location.getLongitude(), true);
        }

        @Override
        public void onProviderDisabled(String provider)
        {
            // TODO Auto-generated method stub

        }

        @Override
        public void onProviderEnabled(String provider)
        {
            // TODO Auto-generated method stub

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            // TODO Auto-generated method stub

        }

    };


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        setResult();
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:

                setResult();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setResult()
    {
        if (currentLocation != null)
        {
            Intent intent = new Intent();
            intent.putExtra(Violation.ADDRESS_VIOLATION_FIELD_NAME, address);
            intent.putExtra(Violation.LONGITUDE_FIELD_NAME, currentLocation.getLongitude());
            intent.putExtra(Violation.LATITUDE_FIELD_NAME, currentLocation.getLatitude());
            setResult(RESULT_OK, intent);
        }
    }

    ItemizedIconOverlay.OnItemGestureListener<OverlayItem> myOnItemGestureListener = new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>()
    {

        @Override
        public boolean onItemLongPress(int arg0, OverlayItem arg1)
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public boolean onItemSingleTapUp(int index, OverlayItem item)
        {
            return true;
        }

    };

    @Override
    protected void onResume()
    {
        // TODO Auto-generated method stub
        super.onResume();
        myLocationOverlay.enableMyLocation();
        //myLocationOverlay.enableCompass();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, myLocationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, myLocationListener);
    }

    @Override
    protected void onPause()
    {
        // TODO Auto-generated method stub
        super.onPause();
        myLocationOverlay.disableMyLocation();
//        myLocationOverlay.disableCompass();
        locationManager.removeUpdates(myLocationListener);
    }

}