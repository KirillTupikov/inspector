package com.inspector.form.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.inspector.R;
import com.inspector.data.Car;
import com.inspector.managers.ImageManager;

/**
 * Created by kirill.tupikov on 10/2/13.
 */
public class SnapshotViewActivity extends Activity
{

    private ImageView imageCar;
    private Button buttonReady;
    private Button buttonDelete;
    private String ImagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_snapshot_view);

        Intent intent = getIntent();
        ImagePath = (String) intent.getStringExtra(Car.PHOTO_CAR_FIELD_NAME);

        setUpControl();
    }

    private void setUpControl()
    {
        imageCar = (ImageView) findViewById(R.id.imageCar);

        imageCar.setImageBitmap(BitmapFactory.decodeFile(ImageManager.getInstance().getDirectory().getPath() + "/" + ImagePath));

        buttonReady = (Button) findViewById(R.id.buttonReady);
        buttonReady.setOnClickListener(onClickReady);

        buttonDelete = (Button) findViewById(R.id.buttonDelete);
        buttonDelete.setOnClickListener(onClickDelete);
    }

    private View.OnClickListener onClickReady = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            finish();
        }
    };

    private View.OnClickListener onClickDelete = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if (ImageManager.getInstance().deleteImage(ImagePath))
            {
                Intent intent = new Intent();
                intent.putExtra(Car.PHOTO_CAR_FIELD_NAME, ImagePath);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    };
}
