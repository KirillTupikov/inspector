package com.inspector.form.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.inspector.R;
import com.inspector.Setting;
import com.inspector.data.Employee;
import com.inspector.exception.MobileServerAPIException;
import com.inspector.form.dialog.ButtonDialog;
import com.inspector.managers.Preferences;
import com.inspector.managers.SessionManager;
import com.inspector.net.data.request.InspectorLoginRequest;
import com.inspector.net.data.response.InspectorLoginResponse;
import com.inspector.utils.NfcUtil;


//@ContentView(R.layout.activity_inspector_login)
public class InspectorLoginActivity extends BaseActivity
{
    private NfcUtil nfcUtil;
//
//    @InjectView(R.id.TechnicianLoginAct_LoginButton)
//    Button loginButton;
//    @InjectView(R.id.TechnicianLoginAct_LoginDebugLayout)
//    LinearLayout loginDebugLayout;

    private Button TechnicianLoginAct;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_inspector_login);

        nfcUtil = new NfcUtil(this);

        checkNFCStatus();

        TechnicianLoginAct = (Button) findViewById(R.id.TechnicianLoginAct_LoginButton);
        TechnicianLoginAct.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                try
                {
                    login("ez9r5khloj");
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    private void startTechnicianAct()
    {
        finish();
        //ActivityLauncher.showMainAct(this);
    }


    private void login(String nfcId) throws Exception
    {

        if (checkNetworkStatus(false))
        {
            //final MobileServerApi mobileServerApi = new MobileServerApi();

            Thread processMobileServerApiLoginThread = new Thread()
            {
                @Override
                public void run()
                {

                    showDialog(WAITING_DIALOG,
                            getString(R.string.TechnicianLoginAct_Dialog_title_LoginPerforming),
                            getString(R.string.TechnicianLoginAct_Dialog_description_LoginPerforming), null, null);

                    try
                    {
                        InspectorLoginResponse inspectorLoginResponse = mobileServerApi.authenticateRequest(new InspectorLoginRequest("ez9r5khloj"));


                        SessionManager.getInstance().openSession(context,
                                inspectorLoginResponse.getData().getToken(),
                                inspectorLoginResponse.getData().getFio(),
                                inspectorLoginResponse.getData().getUserName());

                        stopDialog();
                        startTechnicianAct();
                    }
                    catch (MobileServerAPIException e)
                    {
                        if (e.getErrorCode() == MobileServerAPIException.SERVER_INTERNAL)//серверные проблемы
                        {
                            showDialog(ERROR_DIALOG,
                                    getString(R.string.Dialog_Exception_title_server_internal),
                                    getString(R.string.Dialog_Exception_description_server_internal),
                                    new ButtonDialog(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {

                                        }
                                    }, getString(R.string.Dialogs_continue)), null);
                        }
                        else
                            if (e.getErrorCode() == MobileServerAPIException.ACCESS_DENY)//серверные проблемы
                            {
                                showDialog(ERROR_DIALOG,
                                        getString(R.string.Dialog_Exception_title_access_deny),
                                        getString(R.string.Dialog_Exception_description_access_deny),
                                        new ButtonDialog(new Runnable()
                                        {
                                            @Override
                                            public void run()
                                            {

                                            }
                                        }, getString(R.string.Dialogs_continue)), null);
                            }
                            else
                                stopDialog();

                        e.printStackTrace();
                    }
                    catch (Exception e)
                    {
                        stopDialog();

                        e.printStackTrace();
                    }
                }
            };
            processMobileServerApiLoginThread.start();
            //запрашиваем авторизацию
        }
        else
        {
            //try to auth offline
            Employee technician = Preferences.getLastAuthTechnician(this);
            if (technician != null && nfcId.equals(technician.getCardUid()))
            {
                SessionManager.getInstance().openSession(context,
                        technician.getUserName(),
                        technician.getFio(),
                        technician.getCardUid());
                startTechnicianAct();
            }
            else
            {
                //Utils.showToastMessage(this, R.string.InternetConnectionError);
            }
        }
    }


    public void onLoginButtonClick(View view) throws Exception
    {
        String debugCardId = Preferences.getTechnicianDebugCardId(this);
        if (TextUtils.isEmpty(debugCardId))
        {
            debugCardId = Setting.TECHNICIAN_CARD_ID;
        }
        login(debugCardId);
    }


    @Override
    protected void onResume()
    {
        super.onResume();
        nfcUtil.enableForeground();

        if (nfcUtil.getNfc() != null && !nfcUtil.getNfc().isEnabled())
        {
            Toast.makeText(getApplicationContext(), "Please activate NFC and press Back to return to the application!", Toast.LENGTH_LONG).show();
            startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
            return;
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        nfcUtil.disableForeground();
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        String nfcId = nfcUtil.getTagId(intent);
        if (nfcId != null)
        {
            try
            {
                login(nfcId);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
