package com.inspector.form;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.inspector.data.Car;
import com.inspector.data.Violation;
import com.inspector.form.activity.AboutProgramActivity;
import com.inspector.form.activity.ExitActivity;
import com.inspector.form.activity.InspectorLoginActivity;
import com.inspector.form.activity.OpenStreetMapActivity;
import com.inspector.form.activity.HistoryViolationActivity;
import com.inspector.form.activity.HotLineActivity;
import com.inspector.form.activity.MainActivity;
import com.inspector.form.activity.NotificationActivity;
import com.inspector.form.activity.RegistrationViolationActivity;
import com.inspector.form.activity.SnapshotCarActivity;
import com.inspector.form.activity.SnapshotViewActivity;

/**
 * Created by kirill.tupikov on 11/8/13.
 */
public class ActivityLauncher
{

    public static void showMainAct(Context context)
    {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    public static void showNotificationAct(Context context, int violationID)
    {
        Intent intent = new Intent(context, NotificationActivity.class);
        intent.putExtra(Violation.VIOLATION, violationID);
        context.startActivity(intent);
    }

    public static void showRegistrationViolationAct(Context context)
    {
        Intent intent = new Intent(context, RegistrationViolationActivity.class);
        context.startActivity(intent);
    }

    public static void showSnapshotCarAct(Context context, Violation violation)
    {
        Intent intent = new Intent(context, SnapshotCarActivity.class);
        intent.putExtra(Violation.VIOLATION, violation);
        context.startActivity(intent);
    }

    public static void showHotLineAct(Context context)
    {
        Intent intent = new Intent(context, HotLineActivity.class);
        context.startActivity(intent);
    }

    public static void showHistoryViolationAct(Context context)
    {
        Intent intent = new Intent(context, HistoryViolationActivity.class);
        context.startActivity(intent);
    }

    public static void showOpenStreetMapActForResult(Activity context, int requestCode)
    {
        Intent intent = new Intent(context, OpenStreetMapActivity.class);
        context.startActivityForResult(intent, requestCode);
    }

    public static void showSnapshotViewActForResult(Activity context, int requestCode, String path)
    {
        Intent intent = new Intent(context, SnapshotViewActivity.class);
        intent.putExtra(Car.PHOTO_CAR_FIELD_NAME, path);
        context.startActivityForResult(intent, requestCode);
    }

    public static void showInspectorLoginAct(Context context)
    {
        Intent intent = new Intent(context, InspectorLoginActivity.class);
        context.startActivity(intent);
    }

    public static void ExitAct(Context context)
    {
        Intent intent = new Intent(context, ExitActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void showAboutProgram(Context context)
    {
        Intent intent = new Intent(context, AboutProgramActivity.class);
        context.startActivity(intent);
    }
}
