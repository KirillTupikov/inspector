package com.inspector.form.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import com.inspector.R;
import com.inspector.exception.MobileServerAPIException;
import com.inspector.form.ActivityLauncher;
import com.inspector.managers.DataBaseManager;
import com.inspector.managers.SessionManager;
import com.inspector.net.MobileServerApi;
import com.inspector.net.data.request.GetHistoryRequest;
import com.inspector.net.data.response.GetHistoryResponse;


/**
 * Created by kirill.tupikov on 10/4/13.
 */
public class PreferenceApplication extends PreferenceActivity
{

    public final static String PREFERENCE_APPLICATION = "preference_application";

    private Preference preference;

    private Handler handler;

    private MobileServerApi mobileServerApi;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference_application);

        preference = (Preference) findPreference("loadHistory");
        preference.setOnPreferenceClickListener(onPreferenceClickListener);

        mobileServerApi = new MobileServerApi();

        handler = new Handler();
    }

    private Preference.OnPreferenceClickListener onPreferenceClickListener = new Preference.OnPreferenceClickListener()
    {
        @Override
        public boolean onPreferenceClick(Preference preference)
        {

            if (!SessionManager.getInstance().getToken().isEmpty())
            {
                final ProgressDialog dialog = ProgressDialog.show(PreferenceApplication.this,
                        getString(R.string.PreferenceApplication_dialog_title_load_history),
                        getString(R.string.PreferenceApplication_dialog_description_load_history), true);

                handler.post(new Runnable()
                {
                    public void run()
                    {
                        dialog.hide();

                        Thread processMobileServerApiSendViolationThread = new Thread()
                        {
                            @Override
                            public void run()
                            {
                                try
                                {
                                    GetHistoryResponse getHistoryResponse = mobileServerApi.getHistory(new GetHistoryRequest(SessionManager.getInstance().getToken(), "edc-gibdd"));
                                    DataBaseManager.getInstance().addViolationsHistory(getHistoryResponse.getData());
                                }
                                catch (MobileServerAPIException e)
                                {
                                    e.printStackTrace();
                                }
                                catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                                finally
                                {
                                    handler.post(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            dialog.dismiss();
                                        }
                                    });
                                }
                            }
                        };
                        processMobileServerApiSendViolationThread.start();
                    }
                });
            }
            else
            {
                ActivityLauncher.showInspectorLoginAct(PreferenceApplication.this);
            }

            return true;
        }
    };


}
