package com.inspector.form.activity;

import android.os.Bundle;

import com.inspector.R;

/**
 * Created by kirill.tupikov on 11/11/13.
 */
public class HotLineActivity extends BaseActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_hot_line);

        setUI();
    }

    private void setUI()
    {
        setTitleActionBar(getString(R.string.HotLine_label_hot_line));
    }
}
