package com.inspector.form.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.inspector.R;
import com.inspector.Setting;
import com.inspector.form.ActivityLauncher;
import com.inspector.form.dialog.ButtonDialog;
import com.inspector.managers.DataBaseManager;
import com.inspector.managers.SessionManager;


/**
 * Created by kirill.tupikov on 11/8/13.
 */
public class MainActivity extends BaseActivity
{
    private Button buttonRegistrationViolation;
    private Button buttonHistoryViolation;
    private Button buttonHotLine;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        DataBaseManager.init(context);

        setUI();

        if (!SessionManager.getInstance().hasToken())
        {
            ActivityLauncher.showInspectorLoginAct(context);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if (!SessionManager.getInstance().getFio().isEmpty())
            setTitleActionBar(SessionManager.getInstance().getFio());
        else
            setTitleActionBar(getString(R.string.MainActivity_title_inspector_name));
//
//        if (!SessionManager.getInstance().hasToken())
//        {
//            ActivityLauncher.showInspectorLoginAct(context);
//        }
    }

    private void setUI()
    {
        setTitleActionBar(getString(R.string.MainActivity_title_inspector_name));
        setIconActionBar(R.drawable.ic_inspector);
        enabledButtonHome();

        buttonRegistrationViolation = (Button) findViewById(R.id.buttonRegistrationViolation);
        buttonRegistrationViolation.setOnClickListener(onClickButtonRegistrationViolation);

        buttonHistoryViolation = (Button) findViewById(R.id.buttonHistoryViolation);
        buttonHistoryViolation.setOnClickListener(onClickButtonHistoryViolation);

        buttonHotLine = (Button) findViewById(R.id.buttonHotLine);
        buttonHotLine.setOnClickListener(onClickButtonHotLine);
    }

    private View.OnClickListener onClickButtonRegistrationViolation = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            ActivityLauncher.showRegistrationViolationAct(context);
        }
    };

    private View.OnClickListener onClickButtonHistoryViolation = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            ActivityLauncher.showHistoryViolationAct(context);
        }
    };

    private View.OnClickListener onClickButtonHotLine = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            // ActivityLauncher.showHotLineAct(context);

            showDialog(INFO_DIALOG,
                    getString(R.string.HotLine_label_hot_line),
                    getString(R.string.HotLine_dialog_description_hot_line, Setting.PHONE_NUMBER_HOT_LINE),
                    new ButtonDialog(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            String url = "tel:" + Setting.PHONE_NUMBER_HOT_LINE;
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                            startActivity(intent);
                        }
                    }, getString(R.string.Dialogs_continue)),
                    new ButtonDialog(new Runnable()
                    {
                        @Override
                        public void run()
                        {

                        }
                    }, getString(R.string.Dialogs_cancel)));
        }
    };


}
