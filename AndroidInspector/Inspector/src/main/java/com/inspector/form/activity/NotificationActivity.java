package com.inspector.form.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.inspector.R;
import com.inspector.data.Violation;
import com.inspector.exception.MobileServerAPIException;
import com.inspector.form.ActivityLauncher;
import com.inspector.form.dialog.ButtonDialog;
import com.inspector.managers.DataBaseManager;
import com.inspector.managers.SessionManager;
import com.inspector.net.data.request.ViolationRequest;
import com.inspector.net.data.response.ViolationResponse;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

/**
 * Created by Helm on 11.11.13.
 */
public class NotificationActivity extends BaseActivity
{
    private Button buttonPrint;
    private Button buttonSend;
    private ImageButton buttonEditNumberCar;

    private TextView labelViolationTitle;
    private TextView labelViolationDescription;
    private TextView labelNumberCar;
    private TextView labelDate;
    private TextView labelInspector;

    private EditText protocolNumberEditText;

    private Violation violation;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_notification);

        try
        {
            violation = DataBaseManager.getInstance().getViolationByIndex(getIntent().getIntExtra(Violation.VIOLATION, 0));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        setUI();
    }

    private void setUI()
    {
        setTitleActionBar(getString(R.string.NotificationActivity_label_registration_violation));

        labelViolationTitle = (TextView) findViewById(R.id.labelViolationTitle);
        labelViolationTitle.setText(violation.getArticle().getTitleArticle());

        labelViolationDescription = (TextView) findViewById(R.id.labelViolationDescription);
        labelViolationDescription.setText(violation.getArticle().getDescriptionArticle());

        labelNumberCar = (TextView) findViewById(R.id.labelNumberCar);
        labelNumberCar.setText(violation.getCar().getNumberCar());

        labelDate = (TextView) findViewById(R.id.labelDate);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm");
        String date = simpleDateFormat.format(violation.getDate());
        labelDate.setText(date);

        labelInspector = (TextView) findViewById(R.id.labelInspector);
        if (!SessionManager.getInstance().getFio().isEmpty())
            labelInspector.setText(SessionManager.getInstance().getFio());

        buttonPrint = (Button) findViewById(R.id.buttonPrint);
        buttonPrint.setOnClickListener(onClickButtonPrint);

        buttonSend = (Button) findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(onClickButtonSend);

        if (violation.getStatus().equals(Violation.VIOLATION_COMPLETE))
            buttonSend.setVisibility(View.INVISIBLE);

        buttonEditNumberCar = (ImageButton) findViewById(R.id.buttonEditNumberCar);
        buttonEditNumberCar.setOnClickListener(onClickButtonEditNumberCar);

        InputFilter filterProtocol = new InputFilter()
        {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend)
            {

                String strToReplace = source.subSequence(start, end).toString();
                if (!TextUtils.isEmpty(strToReplace) && (dstart == 2 || dstart == 5) && !strToReplace.substring(0, 1).equals(" "))
                {
                    strToReplace = " " + strToReplace;
                }

                String checkedText = dest.subSequence(0, dstart).toString() +
                        strToReplace +
                        dest.subSequence(dend, dest.length()).toString();
                String pattern = getPattern();

                if (!Pattern.matches(pattern, checkedText))
                {
                    return "";
                }
                return strToReplace;
            }

            private String getPattern()
            {
                return "\\d{1,2}||\\d{1,2} [А-Я]{0,2}||\\d{1,2} [А-Я]{0,2} ||\\d{1,2} [А-Я]{0,2} \\d{0,7}";
            }
        };

        protocolNumberEditText = (EditText) findViewById(R.id.ProtocolNumberEditText);
        protocolNumberEditText.setFilters(new InputFilter[]{filterProtocol});
        protocolNumberEditText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

        if (violation.getProtocolNumber() != null)
            protocolNumberEditText.setText(violation.getProtocolNumber());
    }

    private View.OnClickListener onClickButtonPrint = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {

        }
    };

    private View.OnClickListener onClickButtonSend = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if (checkNetworkStatus(false))
            {
                Thread processMobileServerApiSendViolationThread = new Thread()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            showDialog(WAITING_DIALOG,
                                    getString(R.string.NotificationActivity_Dialog_title_sending_violation),
                                    getString(R.string.NotificationActivity_Dialog_description_sending_violation), null, null);

                            if (SessionManager.getInstance().getToken().equals(""))
                            {
                                //stopDialog();
                                showDialog(WARNING_DIALOG,
                                        getString(R.string.Dialog_Exception_title_not_login),
                                        getString(R.string.Dialog_Exception_description_not_login),
                                        new ButtonDialog(new Runnable()
                                        {
                                            @Override
                                            public void run()
                                            {
                                                ActivityLauncher.showInspectorLoginAct(context);
                                            }
                                        }, getString(R.string.Dialogs_continue)), null);
                                return;
                            }

                            if (protocolNumberEditText.getText().toString().equals(""))
                            {
                                showDialog(WARNING_DIALOG,
                                        getString(R.string.Dialog_Exception_title_not_protocol_number),
                                        getString(R.string.Dialog_Exception_description_not_protocol_number),
                                        null, null);

                                return;
                            }
                            violation.setProtocolNumber(protocolNumberEditText.getText().toString());
                            violation.setVehicleNumber(labelNumberCar.getText().toString());
                            violation.setCode(violation.getArticle().getTitleArticle());
                            violation.setTitle(violation.getArticle().getDescriptionArticle());

                            //Создаём правонарушение
                            DataBaseManager.getInstance().createViolation(violation);//обновляем правнонарушение и пишем в базу

                            ViolationResponse violationResponse = mobileServerApi.createViolationRequest(new ViolationRequest(SessionManager.getInstance().getToken(), SessionManager.getInstance().getUserName(), violation));

                            violation.setViolationID(violationResponse.getData());

                            violation = mobileServerApi.violationPhotoAttachRequest(context, violation);

                            violation.setStatus(Violation.VIOLATION_COMPLETE);
                            DataBaseManager.getInstance().createViolation(violation);//обновляем правнонарушение

                            showDialog(INFO_DIALOG, getString(R.string.NotificationActivity_Dialog_title_send_violation), getString(R.string.NotificationActivity_Dialog_description_send_violation), null, null);
                        }
                        catch (MobileServerAPIException e)
                        {
                            if (e.getErrorCode() == MobileServerAPIException.SESSION_EXPIRED)//сессия окончена, необходимо обновиться
                            {
                                showDialog(ERROR_DIALOG,
                                        getString(R.string.Dialog_Exception_title_session_expired),
                                        getString(R.string.Dialog_Exception_description_session_expired),
                                        new ButtonDialog(new Runnable()
                                        {
                                            @Override
                                            public void run()
                                            {
                                                ActivityLauncher.showInspectorLoginAct(context);
                                            }
                                        }, getString(R.string.Dialogs_continue)),
                                        new ButtonDialog(new Runnable()
                                        {
                                            @Override
                                            public void run()
                                            {

                                            }
                                        }, getString(R.string.Dialogs_cancel)));
                            }
                            else
                                if (e.getErrorCode() == MobileServerAPIException.SERVER_INTERNAL)//серверные проблемы
                                {
                                    showDialog(ERROR_DIALOG,
                                            getString(R.string.Dialog_Exception_title_server_internal),
                                            getString(R.string.Dialog_Exception_description_server_internal),
                                            new ButtonDialog(new Runnable()
                                            {
                                                @Override
                                                public void run()
                                                {

                                                }
                                            }, getString(R.string.Dialogs_continue)), null);
                                }
                                else
                                    if (e.getErrorCode() == MobileServerAPIException.ACCESS_DENY)//ошибка авторизации
                                    {
                                        showDialog(ERROR_DIALOG,
                                                getString(R.string.Dialog_Exception_title_access_deny),
                                                getString(R.string.Dialog_Exception_description_access_deny),
                                                new ButtonDialog(new Runnable()
                                                {
                                                    @Override
                                                    public void run()
                                                    {
                                                        ActivityLauncher.showInspectorLoginAct(context);
                                                    }
                                                }, getString(R.string.Dialogs_continue)), null);
                                    }
                                    else
                                        stopDialog();
                        }
                        catch (Exception e)
                        {
                            stopDialog();
                            e.printStackTrace();
                        }
                    }
                };
                processMobileServerApiSendViolationThread.start();
            }
        }
    };

    private View.OnClickListener onClickButtonEditNumberCar = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.edit_text_control, null);
            final EditText editNumberTC = (EditText) layout.findViewById(R.id.editNumberTC);

            InputFilter filterVehicleNumber = new InputFilter()
            {
                public CharSequence filter(CharSequence source, int start, int end,
                                           Spanned dest, int dstart, int dend)
                {
                    for (int i = start; i < end; i++)
                    {
                        if (!(Character.isLetterOrDigit(source.charAt(i)) && dest.length() <= 10))
                        {
                            return "";
                        }
                    }
                    return null;
                }
            };

            editNumberTC.setFilters(new InputFilter[]{filterVehicleNumber});
            editNumberTC.setInputType(InputType.TYPE_NULL);
            editNumberTC.setCursorVisible(true);

            editNumberTC.setText(labelNumberCar.getText());
            new AlertDialog.Builder(context)
                    .setTitle(getString(R.string.NotificationActivity_Dialog_title_number_tc))
                    .setMessage(R.string.NotificationActivity_Dialog_description_number_tc)
                    .setView(layout)
                    .setPositiveButton(getString(R.string.Dialogs_continue), new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int whichButton)
                        {
                            try
                            {
                                labelNumberCar.setText(editNumberTC.getText().toString());
                            }
                            catch (NullPointerException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }).setNegativeButton(getString(R.string.Dialogs_cancel), new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int whichButton)
                {
                    // Do nothing.
                }
            }).show();
        }
    };

    @Override
    protected void onResume()
    {
        if (!SessionManager.getInstance().getFio().isEmpty())
            labelInspector.setText(SessionManager.getInstance().getFio());
        super.onResume();
    }

}
