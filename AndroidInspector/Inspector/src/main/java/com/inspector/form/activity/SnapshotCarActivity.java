package com.inspector.form.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.inspector.R;
import com.inspector.Setting;
import com.inspector.ThisApplication;
import com.inspector.data.Car;
import com.inspector.data.ImageCar;
import com.inspector.data.Violation;
import com.inspector.form.ActivityLauncher;
import com.inspector.gui.VehicleNumberKeyboardListener;
import com.inspector.managers.DataBaseManager;
import com.inspector.managers.ImageManager;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 11/11/13.
 */
public class SnapshotCarActivity extends BaseActivity
{

    private Button buttonSendViolation;
    private Button buttonGallery;

    private EditText addNumberCar;

    private Violation violation;

    private ImageButton addSnapshotButtonImage;
    private ArrayList<ImageCar> imageCarNumber;

    private TableLayout snapshotTable;

    private int maxSize;

    private KeyboardView keyboardView;
    private Keyboard ruKeyboard;
    private Keyboard enKeyboard;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_snapshot_car);

        violation = (Violation) getIntent().getSerializableExtra(Violation.VIOLATION);
        imageCarNumber = new ArrayList<ImageCar>();

        maxSize = ThisApplication.getInstance().getMaximumSizeImage();

        setUI();

        setUpKeyboard();
    }

    private void setUI()
    {
        setTitleActionBar(getString(R.string.SnapshotCar_label_registration_violation));

        InputFilter filterVehicleNumber = new InputFilter()
        {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend)
            {
                for (int i = start; i < end; i++)
                {
                    if (!(Character.isLetterOrDigit(source.charAt(i)) && dest.length() <= 10))
                    {
                        return "";
                    }
                }
                return null;
            }
        };

        addNumberCar = (EditText) findViewById(R.id.addNumberCar);
        addNumberCar.setFilters(new InputFilter[]{filterVehicleNumber});
        addNumberCar.setInputType(InputType.TYPE_NULL);
        addNumberCar.setCursorVisible(true);

        addNumberCar.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                setKeyboardVisibility(hasFocus && addNumberCar.isEnabled());
            }
        });
        addNumberCar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (keyboardView.getVisibility() == View.GONE)
                {
                    keyboardView.setVisibility(View.VISIBLE);
                }
            }
        });

        buttonSendViolation = (Button) findViewById(R.id.buttonSendViolation);
        buttonSendViolation.setOnClickListener(onClickButtonSendViolation);

        buttonGallery = (Button) findViewById(R.id.buttonGallery);
        buttonGallery.setOnClickListener(onClickButtonGallery);

        addSnapshotButtonImage = (ImageButton) findViewById(R.id.addSnapshotButtonImage);
        addSnapshotButtonImage.setOnClickListener(onClickButtonAddSnapshot);

        snapshotTable = (TableLayout) findViewById(R.id.snapshotTable);

        if (!imageCarNumber.isEmpty())
        {
            for (ImageCar imageCar : imageCarNumber)
            {
                displayCheck(BitmapFactory.decodeFile(ImageManager.getInstance().getDirectory().getPath() + "/" + imageCar.getLocalPathImage()), imageCar.getLocalPathImage());
            }
        }
    }

    private void displayCheck(Bitmap bitmap, String fileName)
    {
        TableRow row = (TableRow) snapshotTable.getChildAt(snapshotTable.getChildCount() - 1);
        ImageView imageView;

        if (row.getChildCount() == 1)
        {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setBackgroundResource(R.drawable.ic_photo);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            linearLayout.setGravity(Gravity.CENTER);
            linearLayout.setLayoutParams(layoutParams);

            layoutParams.leftMargin = 15;

            imageView = new ImageView(context);
            imageView.setImageBitmap(ImageManager.getInstance().resizeImage(bitmap, maxSize));
            imageView.setOnClickListener(onClickImage);
            imageView.setTag(fileName);

            LinearLayout.LayoutParams layoutParamsImage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            imageView.setLayoutParams(layoutParamsImage);

            linearLayout.addView(imageView);

//            LinearLayout linearLayoutSeparator = new LinearLayout(context);
//            LinearLayout.LayoutParams layoutParamsSeparator = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            layoutParamsSeparator.weight = (float) 1.0;
//            linearLayoutSeparator.setLayoutParams(layoutParamsSeparator);
//            row.addView(linearLayoutSeparator, layoutParamsSeparator);

            row.addView(linearLayout, new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        }
        else
        {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setBackgroundResource(R.drawable.ic_photo);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.rightMargin = 15;
            linearLayout.setGravity(Gravity.CENTER);
            linearLayout.setLayoutParams(layoutParams);

            imageView = new ImageView(context);
            imageView.setImageBitmap(ImageManager.getInstance().resizeImage(bitmap, maxSize));
            imageView.setOnClickListener(onClickImage);
            imageView.setTag(fileName);

            LinearLayout.LayoutParams layoutParamsImage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            imageView.setLayoutParams(layoutParamsImage);

            linearLayout.addView(imageView);

            TableRow.LayoutParams layoutParamsTable = new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
            layoutParamsTable.rightMargin = 15;
            TableRow bufRow = new TableRow(context);
            bufRow.setOrientation(LinearLayout.HORIZONTAL);
            bufRow.setPadding(15, 15, 15, 0);

            bufRow.addView(linearLayout, layoutParamsTable);

            snapshotTable.addView(bufRow);
        }
    }

    private View.OnClickListener onClickImage = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            ActivityLauncher.showSnapshotViewActForResult(SnapshotCarActivity.this, Setting.ACTIVITY_SNAPSHOT_VIEW_COD_REQUEST, (String) view.getTag());
        }
    };

    private View.OnClickListener onClickButtonSendViolation = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if (imageCarNumber.size() == 0)
            {
                showDialog(WARNING_DIALOG,
                        getString(R.string.SnapshotCar_dialog_label_not_photo),
                        getString(R.string.SnapshotCar_dialog_description_add_photo), null, null);
                return;
            }

            if (addNumberCar.getText().toString().isEmpty())
            {
                showDialog(WARNING_DIALOG,
                        getString(R.string.SnapshotCar_dialog_label_not_car_number),
                        getString(R.string.SnapshotCar_dialog_description_not_car_number), null, null);
                return;
            }

            try
            {
                Car car = new Car(addNumberCar.getText().toString());
                DataBaseManager.getInstance().createCar(car);

                for (ImageCar imageCar : imageCarNumber)
                {
                    imageCar.setCar(car);
                    DataBaseManager.getInstance().createImageCar(imageCar);
                }

                violation.setCar(car);

                DataBaseManager.getInstance().createViolation(violation);
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }

            ActivityLauncher.showNotificationAct(context, violation.getId());
        }
    };

    private View.OnClickListener onClickButtonAddSnapshot = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if (keyboardView.getVisibility() == View.VISIBLE)
            {
                keyboardView.setVisibility(View.GONE);
            }

            if (ThisApplication.getInstance().getMaximumSizeImage() == 0)
            {
                ThisApplication.getInstance().setMaximumSizeImage(addSnapshotButtonImage.getWidth());
                maxSize = ThisApplication.getInstance().getMaximumSizeImage();
            }

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, Setting.RESULT_LOAD_IMAGE_CAMERA);
        }
    };

    private View.OnClickListener onClickButtonGallery = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if (keyboardView.getVisibility() == View.VISIBLE)
            {
                keyboardView.setVisibility(View.GONE);
            }

            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, Setting.RESULT_LOAD_IMAGE_GALERY);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == Setting.RESULT_LOAD_IMAGE_GALERY)
        {

            if (resultCode == RESULT_OK)
            {
                if (intent == null)
                {
                    Log.d(TAG, "Intent is null Galery");
                }
                else
                {
                    Uri selectedImage = intent.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                    if (bitmap == null)
                    {
                        return;
                    }

                    imageCarNumber.add(new ImageCar(picturePath));
                    displayCheck(bitmap, picturePath);
                }
            }
            else
                if (resultCode == RESULT_CANCELED)
                {
                    Log.d(TAG, "Canceled Galery");
                }
        }
        else
            if (requestCode == Setting.RESULT_LOAD_IMAGE_CAMERA)
            {

                if (resultCode == RESULT_OK)
                {
                    if (intent == null)
                    {
                        Log.d(TAG, "Intent is null Camera");
                    }
                    else
                    {
                        Log.d(TAG, "Photo uri: " + intent.getData());
                        Bundle bndl = intent.getExtras();
                        if (bndl != null)
                        {
                            Object obj = intent.getExtras().get("data");
                            if (obj instanceof Bitmap)
                            {
                                Bitmap bitmap = (Bitmap) obj;
                                Log.d(TAG, "bitmap " + bitmap.getWidth() + " x " + bitmap.getHeight());

                                try
                                {
                                    String picturePath = ImageManager.getInstance().saveGenerateFileUri(bitmap, context);

                                    imageCarNumber.add(new ImageCar(picturePath));
                                    displayCheck(bitmap, picturePath);
                                }
                                catch (IOException e)
                                {
                                    e.printStackTrace();
                                    Log.d(TAG, "Error save BitMap");
                                }
                            }
                        }
                    }
                }
                else
                    if (resultCode == RESULT_CANCELED)
                    {
                        Log.d(TAG, "Canceled Camera");
                    }
            }
            else
                if (requestCode == Setting.ACTIVITY_SNAPSHOT_VIEW_COD_REQUEST)
                {
                    if (resultCode == RESULT_OK)
                    {
                        String deleteImagePath = intent.getStringExtra(Car.PHOTO_CAR_FIELD_NAME);
                        for (ImageCar imageCar : imageCarNumber)
                        {
                            if (imageCar.getLocalPathImage().equals(deleteImagePath))
                            {
                                imageCarNumber.remove(imageCar);
                                break;
                            }
                        }

                        deleteCheck();
                    }
                }
    }

    private void deleteCheck()
    {
        TableRow row = (TableRow) snapshotTable.getChildAt(0);
        row.removeViewsInLayout(1, 1);

        snapshotTable.removeAllViews();
        snapshotTable.addView(row);

        if (!imageCarNumber.isEmpty())
        {
            for (ImageCar imageCar : imageCarNumber)
            {
                displayCheck(BitmapFactory.decodeFile(ImageManager.getInstance().getDirectory().getPath() + "/" + imageCar.getLocalPathImage()), imageCar.getLocalPathImage());
            }
        }
    }

    //Очищаем фотографии машины
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyboardView.getVisibility() == View.VISIBLE)
            keyboardView.setVisibility(View.GONE);
        else
        {
            if (imageCarNumber.size() != 0)
                for (ImageCar imageCar : imageCarNumber)
                    ImageManager.getInstance().deleteImage(imageCar.getLocalPathImage());

            return super.onKeyDown(keyCode, event);
        }
        return false;
    }

    //Очищаем фотографии машины
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                if (imageCarNumber.size() != 0)
                    for (ImageCar imageCar : imageCarNumber)
                        ImageManager.getInstance().deleteImage(imageCar.getLocalPathImage());
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void setUpKeyboard()
    {
        keyboardView = (KeyboardView) findViewById(R.id.keyboardView);
        ruKeyboard = new Keyboard(this, R.xml.vehicle_number_keyboard);
        enKeyboard = new Keyboard(this, R.xml.vehicle_number_keyboard_en);
        keyboardView.setKeyboard(ruKeyboard);

        VehicleNumberKeyboardListener keyboardListener = new VehicleNumberKeyboardListener(addNumberCar, keyboardView, ruKeyboard, enKeyboard);

        keyboardView.setEnabled(true);
        keyboardView.setPreviewEnabled(false);
        keyboardView.setOnKeyListener(keyboardListener);
        keyboardView.setOnKeyboardActionListener(keyboardListener);
    }

    private void setKeyboardVisibility(boolean hasFocus)
    {
        KeyboardView keyboardView = (KeyboardView) findViewById(R.id.keyboardView);
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        if (hasFocus)
        {
            imm.hideSoftInputFromWindow(addNumberCar.getWindowToken(), 0);
            keyboardView.setVisibility(View.VISIBLE);
        }
        else
        {
            keyboardView.setVisibility(View.GONE);
        }
    }
}