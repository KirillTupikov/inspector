package com.inspector.form.dialog.impl;

import android.content.DialogInterface;

/**
 * Created with IntelliJ IDEA.
 * User: Kirill
 * Date: 27.04.13
 * Time: 11:28
 * To change this template use File | Settings | File Templates.
 */
public interface IModalDialog extends DialogInterface
{

    public void show();

    public void close();

}
