package com.inspector.interfaces;

import android.location.Location;

public interface IGPSCallback
{
    public abstract void onGPSUpdate(Location location);
}
