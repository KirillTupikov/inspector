package com.inspector.interfaces;


import com.inspector.form.dialog.ButtonDialog;

/**
 * Created with IntelliJ IDEA.
 * User: Kirill
 * Date: 26.04.13
 * Time: 17:27
 * To change this template use File | Settings | File Templates.
 */
public interface IDialogSupportedActivity
{

    public static final String WARNING_DIALOG = "warning";
    public static final String ERROR_DIALOG = "error";
    public static final String INFO_DIALOG = "info";
    public static final String WAITING_DIALOG = "waiting";

    public void showDialog(String dialogType, String title, String message, ButtonDialog actionPositive, ButtonDialog actionNegative);

}
