package com.inspector;

/**
 * Created by kirill.tupikov on 11/14/13.
 */
public class Setting
{

    //private static String HOST = "http://192.168.11.59:8080";
    private static String HOST = "http://192.168.11.74:8080";

    public static final int RESULT_LOAD_IMAGE_GALERY = 1;
    public static final int RESULT_LOAD_IMAGE_CAMERA = 2;
    public static final int ACTIVITY_SNAPSHOT_VIEW_COD_REQUEST = 3;
    public static final int OPEN_STREET_MAP_VIEW_COD_REQUEST = 4;

    public static final String PHONE_NUMBER_HOT_LINE = "+375 (29) 112-76-47";

    public static final String TECHNICIAN_CARD_ID = "technician_card_id";

    public static final String MOBILE_SERVER_API_URL_KEY_CREATE_VIOLATION = HOST + "/remote/policeman/violation/create";
    public static final String MOBILE_SERVER_API_URL_KEY_AUTHENTICATE = HOST + "/remote/policeman/authenticate";
    public static final String MOBILE_SERVER_API_URL_KEY_LOGOUT = HOST + "/remote/policeman/logout";
    public static final String MOBILE_SERVER_API_URL_PHOTO_ATTACH = HOST +"/remote/policeman/violation/generalphoto/attach";
    public static final String MOBILE_SERVER_API_URL_LIST_VIOLATION_ARTICLE = HOST +"/remote/policeman/article/get";
    public static final String MOBILE_SERVER_API_GET_HISTORY = HOST +"/remote/policeman/violation/policemanName/find";

    public static final int OPERATION_TIMEOUT = 5000;

    public static final int DEFAULT_REFRESH_TIMEOUT = 37;
}
