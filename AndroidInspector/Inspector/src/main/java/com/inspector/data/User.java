package com.inspector.data;

/**
 * Created by kirill.tupikov on 11/27/13.
 */
public class User
{
    protected String token;

    protected String userName;

    protected String fio;



    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getFio()
    {
        return fio;
    }

    public void setFio(String fio)
    {
        this.fio = fio;
    }
}
