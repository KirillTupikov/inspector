package com.inspector.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by kirill.tupikov on 11/13/13.
 */
@DatabaseTable(tableName = "violation")
public class Violation implements Serializable
{
    public static final String VIOLATION = "violation";

    public static final String VIOLATION_COMPLETE = "complete";
    public static final String VIOLATION_PROCESSED = "processed";

    public static final String ID_VIOLATION_FIELD_NAME = "id";
    public static final String SUM_VIOLATION_FIELD_NAME = "sum_violation";
    public static final String DATE_VIOLATION_FIELD_NAME = "date_violation";
    public static final String INSPECTOR_FIELD_NAME = "inspector";
    public static final String CAR_VIOLATION_FIELD_NAME = "car_violation";
    public static final String ARTICLE_VIOLATION_FIELD_NAME = "article_violation";
    public static final String ADDRESS_VIOLATION_FIELD_NAME = "address_violation";
    public static final String LATITUDE_FIELD_NAME = "latitude";
    public static final String LONGITUDE_FIELD_NAME = "longitude";
    public static final String STATUS_VIOLATION_FIELD_NAME = "status_violation";
    public static final String REMOTE_ID_VIOLATION_FIELD_NAME = "remote_id_violation";

    public static final String CODE = "code";
    public static final String TITLE = "title";
    public static final String ADDRESS = "address";
    public static final String TC_NUMBER = "tcNumber";
    public static final String PROTOCOL_NUMBER = "protocolNumber";
    public static final String POLICEMAN_NAME = "policemanName";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    @DatabaseField(columnName = ID_VIOLATION_FIELD_NAME, generatedId = true)
    private int id;

    @DatabaseField(columnName = SUM_VIOLATION_FIELD_NAME, dataType = DataType.DOUBLE)
    private double sum;

    @DatabaseField(columnName = DATE_VIOLATION_FIELD_NAME, dataType = DataType.DATE)
    private Date date;

    @DatabaseField(columnName = INSPECTOR_FIELD_NAME, dataType = DataType.STRING)
    private String inspector;

    @SerializedName(ADDRESS)
    @Expose
    @DatabaseField(columnName = ADDRESS_VIOLATION_FIELD_NAME, dataType = DataType.STRING)
    private String address;

    @SerializedName(LONGITUDE)
    @Expose
    @DatabaseField(columnName = LONGITUDE_FIELD_NAME, dataType = DataType.STRING)
    private String longitude;

    @SerializedName(LATITUDE)
    @Expose
    @DatabaseField(columnName = LATITUDE_FIELD_NAME, dataType = DataType.STRING)
    private String latitude;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = CAR_VIOLATION_FIELD_NAME)
    private Car car;

    @Expose
    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = ARTICLE_VIOLATION_FIELD_NAME)
    private Article article;

    @DatabaseField
    @SerializedName(CODE)
    @Expose
    private String code;

    @DatabaseField
    @SerializedName(TITLE)
    @Expose
    private String title;


    @DatabaseField
    @SerializedName(TC_NUMBER)
    @Expose
    private String vehicleNumber;

    @DatabaseField
    @SerializedName(PROTOCOL_NUMBER)
    @Expose
    private String protocolNumber;

//    @SerializedName(POLICEMAN_NAME)
//    @Expose
//    private String policemanName;

//    @DatabaseField
//    @SerializedName(PROTOCOL_NUMBER)
//    @Expose
//    private Article articleIdBack;

//    @DatabaseField(columnName = "uvdRegion", foreign = true)
//    @SerializedName("uvdRegion")
//    @Expose private UvdRegion uvdRegion;remote_id_violation

    @DatabaseField(columnName = STATUS_VIOLATION_FIELD_NAME, dataType = DataType.STRING)
    private String status;

    @DatabaseField(columnName = REMOTE_ID_VIOLATION_FIELD_NAME, dataType = DataType.STRING)
    private String violationID;

    public Violation()
    {
    }

    public Violation(Date date, String inspector, String address, Article article)
    {
        this.date = date;
        this.inspector = inspector;
        this.address = address;
        this.article = article;
        this.status = VIOLATION_PROCESSED;
    }

    public Violation(Date date, String inspector, String address, Car car, Article article)
    {
        this.date = date;
        this.inspector = inspector;
        this.address = address;
        this.car = car;
        this.article = article;
        this.status = VIOLATION_PROCESSED;
    }

//    public String getPolicemanName()
//    {
//        return policemanName;
//    }
//
//    public void setPolicemanName(String policemanName)
//    {
//        this.policemanName = policemanName;
//    }

    public String getLongitude()
    {
        return longitude;
    }

    public void setLongitude(String longitude)
    {
        this.longitude = longitude;
    }

    public String getLatitude()
    {
        return latitude;
    }

    public void setLatitude(String latitude)
    {
        this.latitude = latitude;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getViolationID()
    {
        return violationID;
    }

    public void setViolationID(String violationID)
    {
        this.violationID = violationID;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getVehicleNumber()
    {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber)
    {
        this.vehicleNumber = vehicleNumber;
    }

    public String getProtocolNumber()
    {
        return protocolNumber;
    }

    public void setProtocolNumber(String protocolNumber)
    {
        this.protocolNumber = protocolNumber;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public double getSum()
    {
        return sum;
    }

    public void setSum(double sum)
    {
        this.sum = sum;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public String getInspector()
    {
        return inspector;
    }

    public void setInspector(String inspector)
    {
        this.inspector = inspector;
    }

    public Car getCar()
    {
        return car;
    }

    public void setCar(Car car)
    {
        this.car = car;
    }

    public Article getArticle()
    {
        return article;
    }

    public void setArticle(Article article)
    {
        this.article = article;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }
}
