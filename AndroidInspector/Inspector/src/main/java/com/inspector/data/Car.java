package com.inspector.data;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 11/13/13.
 */
@DatabaseTable (tableName = "car")
public class Car implements Serializable
{
    public static final String ID_CAR_FIELD_NAME = "id";
    public static final String PHOTO_CAR_FIELD_NAME = "photo_car";
    public static final String NUMBER_CAR_FIELD_NAME = "number_car";

    @DatabaseField(columnName = ID_CAR_FIELD_NAME, generatedId = true)
    private int id;

//    @DatabaseField(id = false, foreign = false, columnName = PHOTO_CAR_FIELD_NAME, dataType = DataType.SERIALIZABLE)
//    private ArrayList<String> photoCar;

    @DatabaseField(columnName = NUMBER_CAR_FIELD_NAME, dataType = DataType.STRING)
    private String NumberCar;


    @ForeignCollectionField(eager = true)
    private ForeignCollection<ImageCar> imageCars;

    public Car()
    {
    }

    public Car(String numberCar)
    {
        NumberCar = numberCar;
    }

    public ArrayList<ImageCar> getImageCars()
    {
        ArrayList<ImageCar> iC = new ArrayList<ImageCar>();
        for (ImageCar imageCar : imageCars)
        {
            iC.add(imageCar);
        }
        return iC;
    }

    public void setImageCars(ArrayList<ImageCar> imageCars)
    {
        this.imageCars.clear();

        for (ImageCar imageCar : imageCars)
        {
            this.imageCars.add(imageCar);
        }
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

//    public ArrayList<String> getPhotoCar()
//    {
//        return photoCar;
//    }
//
//    public void setPhotoCar(ArrayList<String> photoCar)
//    {
//        this.photoCar = photoCar;
//    }

    public String getNumberCar()
    {
        return NumberCar;
    }

    public void setNumberCar(String numberCar)
    {
        NumberCar = numberCar;
    }
}
