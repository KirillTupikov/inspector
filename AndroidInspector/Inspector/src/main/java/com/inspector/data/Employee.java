package com.inspector.data;

import android.text.TextUtils;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class Employee implements Serializable
{
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    protected int externalId;
    @DatabaseField
    protected String positionName;
    @DatabaseField
    protected String userName;
    @DatabaseField
    protected String firstName;
    @DatabaseField
    protected String lastName;
    @DatabaseField
    protected String email;
    @DatabaseField
    protected String phone;
    @DatabaseField
    protected String internalPhone;
    @DatabaseField
    protected String technicianUserName;
    private String cardUid;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getExternalId()
    {
        return externalId;
    }

    public void setExternalId(int externalId)
    {
        this.externalId = externalId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getInternalPhone()
    {
        return internalPhone;
    }

    public void setInternalPhone(String internalPhone)
    {
        this.internalPhone = internalPhone;
    }

    public String getPositionName()
    {
        return positionName;
    }

    public void setPositionName(String positionName)
    {
        this.positionName = positionName;
    }

    public String getTechnicianUserName()
    {
        return technicianUserName;
    }

    public void setTechnicianUserName(String technicianUserName)
    {
        this.technicianUserName = technicianUserName;
    }

    public String getCardUid()
    {
        return cardUid;
    }

    public void setCardUid(String cardUid)
    {
        this.cardUid = cardUid;
    }

    public String getFio()
    {
        return (TextUtils.isEmpty(firstName) ? "" : firstName) + " " + (TextUtils.isEmpty(lastName) ? "" : lastName);
    }

    @Override
    public boolean equals(Object o)
    {
        if (o instanceof Employee)
        {
            if (!TextUtils.isEmpty(userName))
            {
                return userName.equals(((Employee) o).getUserName());
            }
        }
        return false;
    }

    @Override
    public int hashCode()
    {
        return userName.hashCode();
    }
}
