package com.inspector.data;

import android.provider.ContactsContract;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by kirill.tupikov on 11/13/13.
 */
@DatabaseTable(tableName = "article")
public class Article implements Serializable
{
    public static final String ID_ARTICLE_LOCAL_FIELD_NAME = "id_local";
    public static final String ID_ARTICLE_BAK_FIELD_NAME = "id_back";
    public static final String TITLE_ARTICLE_FIELD_NAME = "title_article";
    public static final String DESCRIPTION_ARTICLE_FIELD_NAME = "description_article";

    public static final String ID_ARTICLE_BAK_SERIALIZABLE_NAME = "id";
    public static final String CODE_ARTICLE_BAK_SERIALIZABLE_NAME = "code";
    public static final String TITLE_ARTICLE_BAK_SERIALIZABLE_NAME = "title";

    @DatabaseField(columnName = ID_ARTICLE_LOCAL_FIELD_NAME, generatedId = true)
    private int idLocal;

    @DatabaseField(columnName = TITLE_ARTICLE_FIELD_NAME, dataType = DataType.STRING)
    private String titleArticle;

    @DatabaseField(columnName = DESCRIPTION_ARTICLE_FIELD_NAME, dataType = DataType.STRING)
    private String descriptionArticle;

    @Expose
    @SerializedName(ID_ARTICLE_BAK_SERIALIZABLE_NAME)
    @DatabaseField(columnName = ID_ARTICLE_BAK_FIELD_NAME, dataType = DataType.INTEGER)
    private int idBak;

    @SerializedName(CODE_ARTICLE_BAK_SERIALIZABLE_NAME)
    private String codeJson;

    @SerializedName(TITLE_ARTICLE_BAK_SERIALIZABLE_NAME)
    private String titleJson;

    public Article()
    {
    }

    public Article(String titleArticle, String descriptionArticle)
    {
        this.titleArticle = titleArticle;
        this.descriptionArticle = descriptionArticle;
    }

    public int getIdBak()
    {
        return idBak;
    }

    public void setIdBak(int idBak)
    {
        this.idBak = idBak;
    }

    public String getCodeJson()
    {
        return codeJson;
    }

    public void setCodeJson(String codeJson)
    {
        this.codeJson = codeJson;
    }

    public String getTitleJson()
    {
        return titleJson;
    }

    public void setTitleJson(String titleJson)
    {
        this.titleJson = titleJson;
    }

    public int getId()
    {
        return idLocal;
    }

    public void setId(int id)
    {
        this.idLocal = id;
    }

    public String getTitleArticle()
    {
        return titleArticle;
    }

    public void setTitleArticle(String titleArticle)
    {
        this.titleArticle = titleArticle;
    }

    public String getDescriptionArticle()
    {
        return descriptionArticle;
    }

    public void setDescriptionArticle(String descriptionArticle)
    {
        this.descriptionArticle = descriptionArticle;
    }
}
