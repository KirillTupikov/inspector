package com.inspector.data;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by kirill.tupikov on 12/3/13.
 */

@DatabaseTable(tableName = "image_car")
public class ImageCar implements Serializable
{
    public final static String ID_IMAGE_CAR_FIELD_NAME = "id";
    public final static String LOCAL_PATH_IMAGE_FIELD_NAME = "local_path_image";
    public final static String REMOTE_PATH_IMAGE_FIELD_NAME = "remote_path_image";

    @DatabaseField(columnName = ID_IMAGE_CAR_FIELD_NAME, generatedId = true)
    private int id;

    @DatabaseField(columnName = LOCAL_PATH_IMAGE_FIELD_NAME, dataType = DataType.STRING)
    private String localPathImage;

    @DatabaseField(columnName = REMOTE_PATH_IMAGE_FIELD_NAME, dataType = DataType.STRING)
    private String remotePathImage;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Car car;

    public ImageCar(String localPathImage, String remotePathImage)
    {
        this.localPathImage = localPathImage;
        this.remotePathImage = remotePathImage;
    }

    public ImageCar(String localPathImage)
    {
        this.localPathImage = localPathImage;
    }

    public ImageCar()
    {
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getLocalPathImage()
    {
        return localPathImage;
    }

    public void setLocalPathImage(String localPathImage)
    {
        this.localPathImage = localPathImage;
    }

    public String getRemotePathImage()
    {
        return remotePathImage;
    }

    public void setRemotePathImage(String remotePathImage)
    {
        this.remotePathImage = remotePathImage;
    }

    public Car getCar()
    {
        return car;
    }

    public void setCar(Car car)
    {
        this.car = car;
    }
}
