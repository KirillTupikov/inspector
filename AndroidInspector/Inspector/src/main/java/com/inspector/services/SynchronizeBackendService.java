package com.inspector.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.inspector.Setting;
import com.inspector.ThisApplication;
import com.inspector.data.Violation;
import com.inspector.exception.MobileServerAPIException;
import com.inspector.managers.DataBaseManager;
import com.inspector.managers.SessionManager;
import com.inspector.net.MobileServerApi;
import com.inspector.net.data.request.ViolationRequest;
import com.inspector.net.data.response.ViolationResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by GSTU_Robotics on 09.12.13.
 */
public class SynchronizeBackendService extends Service
{
    final String LOG_TAG = "SynchronizeBackendServiceLog";

    private MobileServerApi mobileServerApi;

    public void onCreate()
    {
        mobileServerApi = new MobileServerApi();
        super.onCreate();
        Log.d(LOG_TAG, "onCreate");
    }

    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.d(LOG_TAG, "onStartCommand");
        someTask();
        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy()
    {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }

    void someTask()
    {
        new Thread(new Runnable()
        {
            public void run()
            {
                while (true)
                {
                    try
                    {
                        TimeUnit.SECONDS.sleep(Setting.DEFAULT_REFRESH_TIMEOUT);// каждый timeout пинаем базу на наличие не отправленных нарушений

                        ArrayList<Violation> violations = DataBaseManager.getInstance().getViolationByStatus(Violation.VIOLATION_PROCESSED);

                        if (violations.size() != 0)
                        {
                            for (Violation violation : violations)
                            {
                                try
                                {
                                    ViolationResponse violationResponse = mobileServerApi.createViolationRequest(new ViolationRequest(SessionManager.getInstance().getToken(),SessionManager.getInstance().getUserName(), violation));

                                    violation.setViolationID(violationResponse.getData());

                                    violation = mobileServerApi.violationPhotoAttachRequest(ThisApplication.getInstance().getApplicationContext(), violation);//? context

                                    violation.setStatus(Violation.VIOLATION_COMPLETE);
                                    DataBaseManager.getInstance().createViolation(violation);//обновляем правнонарушение
                                }
                                catch (MobileServerAPIException e)
                                {
                                    Log.d(LOG_TAG, "MobileServerAPIException: " + e.toString());
                                    e.printStackTrace();
                                }
                                catch (Exception e)
                                {
                                    Log.d(LOG_TAG, "Exception: " + e.toString());
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    catch (InterruptedException e)
                    {
                        Log.d(LOG_TAG, "InterruptedException: " + e.toString());
                        e.printStackTrace();
                    }
                    catch (SQLException e)
                    {
                        Log.d(LOG_TAG, "SQLException: " + e.toString());
                        e.printStackTrace();
                    }
                    catch (Exception e)
                    {
                        Log.d(LOG_TAG, "Exception: " + e.toString());
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }
}
