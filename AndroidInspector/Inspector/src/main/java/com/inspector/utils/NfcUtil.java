package com.inspector.utils;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.NfcA;
import android.util.Log;

public class NfcUtil
{
    private NfcAdapter nfc;
    static final String HEXES = "0123456789ABCDEF";

    private Activity activity;
    private IntentFilter intentFiltersArray[];
    private PendingIntent intent;
    private String techListsArray[][];

    public NfcUtil(Activity activity)
    {
        this.activity = activity;
        nfc = NfcAdapter.getDefaultAdapter(activity.getApplicationContext());
        if (nfc != null)
        {
            intent = PendingIntent.getActivity(activity, 0, new Intent(activity,
                    activity.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

            IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);

//        try {
//            ndef.addDataType("*/*");
//        } catch (IntentFilter.MalformedMimeTypeException e) {
//            throw new RuntimeException("Unable to speciy */* Mime Type", e);
//        }

            intentFiltersArray = new IntentFilter[]{ndef};

            techListsArray = new String[][]{new String[]{NfcA.class.getName(), MifareUltralight.class.getName()}};
            //techListsArray = new String[][] { new String[] { NfcA.class.getName(), NfcB.class.getName() }, new String[] {NfcV.class.getName()} };
        }
    }

    public boolean isNfcEnabled()
    {
        return nfc == null ? false : nfc.isEnabled();
    }

    public static String getHexString(byte[] raw)
    {
        if (raw == null)
        {
            return null;
        }
        final StringBuilder hex = new StringBuilder(2 * raw.length);
        for (final byte b : raw)
        {
            hex.append(HEXES.charAt((b & 0xF0) >> 4))
                    .append(HEXES.charAt((b & 0x0F)));
        }
        return hex.toString();
    }

    public String getTagId(Intent intent)
    {
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        byte[] tagId = tag.getId();
        String tagIdStr = NfcUtil.getHexString(tagId);
        return tagIdStr;
    }

    public void enableForeground()
    {
        Log.d("demo", "Foreground NFC dispatch enabled");
        if (nfc != null)
        {
            nfc.enableForegroundDispatch(activity, intent, intentFiltersArray, null);
        }
    }

    public void disableForeground()
    {
        Log.d("demo", "Foreground NFC dispatch disabled");
        if (nfc != null)
        {
            nfc.disableForegroundDispatch(activity);
        }
    }

    public NfcAdapter getNfc()
    {
        return nfc;
    }

    public static NfcStatus getNfcStatus(Context context)
    {
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);

        if (nfcAdapter == null)
        {
            return NfcStatus.NOT_AVAILABLE;
        }
        else if (!nfcAdapter.isEnabled())
        {
            return NfcStatus.OF;
        }
        else
        {
            return NfcStatus.ON;
        }
    }
}
