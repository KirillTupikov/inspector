package com.inspector.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.widget.Toast;

public class Utils
{

    public static void showToastMessage(Context context, int messageResId)
    {
        Toast.makeText(context, messageResId, Toast.LENGTH_SHORT).show();
    }

    public static boolean isBlank(String value)
    {
        return TextUtils.isEmpty(value) || TextUtils.getTrimmedLength(value) == 0;
    }

    public static final boolean isOnline(Context context)
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info != null)
        {
            return info.isConnected();
        }
        return false;
    }


}
