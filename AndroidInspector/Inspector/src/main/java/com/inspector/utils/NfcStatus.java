package com.inspector.utils;

/**
 * Created with IntelliJ IDEA.
 * User: Kirill
 * Date: 06.03.13
 * Time: 15:48
 * To change this template use File | Settings | File Templates.
 */
public enum NfcStatus
{
    NOT_AVAILABLE, ON, OF;
}
