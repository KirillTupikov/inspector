package com.inspector.exception;

/**
 * Created with IntelliJ IDEA.
 * User: Kirill
 * Date: 22.04.13
 * Time: 15:32
 * To change this template use File | Settings | File Templates.
 */
public class NfcException extends Exception
{

    public NfcException(String detailMessage)
    {
        super(detailMessage);
    }

    public NfcException(String detailMessage, Throwable throwable)
    {
        super(detailMessage, throwable);
    }
}
