package com.inspector.exception;

import com.google.gson.annotations.Expose;

/**
 * Created with IntelliJ IDEA.
 * User: Kirill
 * Date: 02.04.13
 * Time: 17:27
 * To change this template use File | Settings | File Templates.
 */
public class MobileServerAPIException extends Exception
{
    public static final int RESULT_SUCCESS = 0;
    public static final int SERVER_INTERNAL = 500;
    public static final int SIGN_NOT_ALLOWED = 300;
    public static final int ACCESS_DENY = 200;
    public static final int SESSION_EXPIRED = 100;

    @Expose
    protected String messageTitle;
    @Expose
    protected String message;
    @Expose
    protected int errorCode;

    public MobileServerAPIException(String messageTitle, String message, int errorCode)
    {
        this.messageTitle = messageTitle;
        this.message = message;
        this.errorCode = errorCode;
    }

    public String getMessageTitle()
    {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle)
    {
        this.messageTitle = messageTitle;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public int getErrorCode()
    {
        return errorCode;
    }

    public void setErrorCode(int errorCode)
    {
        this.errorCode = errorCode;
    }
}
