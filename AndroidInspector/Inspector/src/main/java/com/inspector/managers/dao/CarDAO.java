package com.inspector.managers.dao;

import com.inspector.data.Car;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 11/13/13.
 */
public class CarDAO extends BaseDaoImpl<Car, Integer>
{
    public CarDAO(ConnectionSource connectionSource, Class<Car> dataClass) throws SQLException
    {
        super(connectionSource, dataClass);
    }

    public ArrayList<Car> getAllCars() throws SQLException
    {
        return new ArrayList<Car>(this.queryForAll());
    }
}
