package com.inspector.managers;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.inspector.ThisApplication;
import com.inspector.services.SynchronizeBackendService;


public class SessionManager
{
    //singleton
    private static SessionManager instance;

    private SynchronizeBackendService service;

    private void SessionManager()
    {
    }

    public static SessionManager getInstance()
    {
        if (instance == null)
        {
            instance = new SessionManager();
        }
        return instance;
    }
    //===

    public static final String PARAM_TOKEN = "token";

    private String token;
    private String fio;
    private String userName;
    private String cardId;
    private int refreshTimeout;
    private int maxViolationImagesCount;
    private int minViolationImagesCount;

    public void openSession(Context context, String token, String fio, String userName)
    {
        setToken(token);
        setFio(fio);
        setUserName(userName);

        context.startService(new Intent(context, SynchronizeBackendService.class));
    }

    public String getToken()
    {
        if (TextUtils.isEmpty(token))
        {
            token = Preferences.getToken(ThisApplication.getInstance().getApplicationContext());
        }
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
        Preferences.setToken(ThisApplication.getInstance().getApplicationContext(), token);
        //reset check time to check new version after login
        Preferences.setCheckNewVersionTime(ThisApplication.getInstance().getApplicationContext(), 0);
    }

    public String getUserName()
    {
        if (TextUtils.isEmpty(userName))
        {
            userName = Preferences.getUserName(ThisApplication.getInstance().getApplicationContext());
        }
        return userName;
    }

    public String getUrlWithNewToken(String url)
    {
        if (!TextUtils.isEmpty(url))
        {
            int tokenPos = url.indexOf(PARAM_TOKEN);
            if (tokenPos > 0)
            {
                url = url.substring(0, tokenPos);
            }
            url += ((url.charAt(url.length() - 1) == '?' || url.charAt(url.length() - 1) == '&') ? "" : "&") + PARAM_TOKEN + "=" + token;
        }
        return url;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
        Preferences.setUserName(ThisApplication.getInstance().getApplicationContext(), userName);
    }

    public String getFio()
    {
        if (TextUtils.isEmpty(fio))
        {
            fio = Preferences.getUserFIO(ThisApplication.getInstance().getApplicationContext());
        }
        return fio;
    }

    public void setFio(String fio)
    {
        this.fio = fio;
        Preferences.setUserFIO(ThisApplication.getInstance().getApplicationContext(), fio);
    }

    public String getCardId()
    {
        if (TextUtils.isEmpty(cardId))
        {
            cardId = Preferences.getUserCardID(ThisApplication.getInstance().getApplicationContext());
        }
        return cardId;
    }

    public void setCardId(String cardId)
    {
        this.cardId = cardId;
        Preferences.setUserCardId(ThisApplication.getInstance().getApplicationContext(), cardId);
    }

    public boolean hasToken()
    {
        return !TextUtils.isEmpty(getToken());
    }

//    public int getRefreshTimeout()
//    {
//        if (refreshTimeout == 0)
//        {
//            refreshTimeout = Preferences.getRefreshTimeOut(ThisApplication.getInstance().getApplicationContext());
//        }
//        if (refreshTimeout < Settings.DEFAULT_REFRESH_TIMEOUT)
//        {
//            refreshTimeout = Settings.DEFAULT_REFRESH_TIMEOUT;
//        }
//        return refreshTimeout;
//    }

    public void setRefreshTimeout(int refreshTimeout)
    {
        Preferences.setRefreshTimeout(ThisApplication.getInstance().getApplicationContext(), refreshTimeout);
        this.refreshTimeout = refreshTimeout;
    }

//    public int getMaxViolationImagesCount()
//    {
//        if (maxViolationImagesCount == 0)
//        {
//            maxViolationImagesCount = Preferences.getMaxViolationImagesCount(ThisApplication.getInstance().getApplicationContext());
//        }
//        if (maxViolationImagesCount == 0)
//        {
//            maxViolationImagesCount = Settings.DEFAULT_VIOLATION_MAX_IMAGES_COUNT;
//        }
//        return maxViolationImagesCount;
//    }

    public void setMaxViolationImagesCount(int maxViolationImagesCount)
    {
        Preferences.setMaxViolationImagesCount(ThisApplication.getInstance().getApplicationContext(), maxViolationImagesCount);
        this.maxViolationImagesCount = maxViolationImagesCount;
    }

//    public int getMinViolationImagesCount()
//    {
//        if (minViolationImagesCount == 0)
//        {
//            minViolationImagesCount = Preferences.getMinViolationImagesCount(ThisApplication.getInstance().getApplicationContext());
//        }
//        if (minViolationImagesCount < Settings.DEFAULT_VIOLATION_MIN_IMAGES_COUNT)
//        {
//            minViolationImagesCount = Settings.DEFAULT_VIOLATION_MIN_IMAGES_COUNT;
//        }
//        return minViolationImagesCount;
//    }

    public void setMinViolationImagesCount(int minViolationImagesCount)
    {
        Preferences.setMinViolationImagesCount(ThisApplication.getInstance().getApplicationContext(), minViolationImagesCount);
        this.minViolationImagesCount = minViolationImagesCount;
    }

    public void close(Context context)
    {
        token = null;
        userName = null;
        fio = null;
        Context contextApp = ThisApplication.getInstance().getApplicationContext();
        Preferences.setToken(contextApp, token);
        Preferences.setUserName(contextApp, userName);
        Preferences.setUserFIO(contextApp, fio);

        context.stopService(new Intent(context, SynchronizeBackendService.class));
    }
}
