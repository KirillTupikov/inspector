package com.inspector.managers.dao;

import com.inspector.data.Article;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 11/13/13.
 */
public class ArticleDAO extends BaseDaoImpl<Article, Integer>
{

    public ArticleDAO(ConnectionSource connectionSource, Class<Article> dataClass) throws SQLException
    {
        super(connectionSource, dataClass);
    }

    public ArrayList<Article> getAllArticles() throws SQLException
    {
        return new ArrayList<Article>(this.queryForAll());
    }

}
