package com.inspector.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.inspector.data.Employee;
import com.inspector.helpers.GsonHelper;

public class Preferences
{
    private static final String PREFERENCE_LAST_AUTH_TECHNICIAN = "PREFERENCE_LAST_AUTH_TECHNICIAN";
    private static final String PREFERENCE_LAST_RECEIVED_MESSAGE_DATE_TIME = "PREFERENCE_LAST_RECEIVED_MESSAGE_DATE_TIME";
    private static final String PREFERENCE_SHOW_POINTINGS = "PREFERENCE_SHOW_POINTINGS";
    public static final String PREFERENCE_SHOW_VIOLATIONS = "PREFERENCE_SHOW_VIOLATIONS";
    public static final String PREFERENCE_SHOW_WRECKERS = "PREFERENCE_SHOW_WRECKERS";
    public static final String PREFERENCE_SHOW_PARKINGS = "PREFERENCE_SHOW_PARKINGS";
    public static final String PREFERENCE_BUILD = "PREFERENCE_BUILD";

    private static final String PREFERENCE_TOKEN = "PREFERENCE_TOKEN";
    private static final String PREFERENCE_USER_FIO = "PREFERENCE_USER_FIO";
    private static final String PREFERENCE_USER_NAME = "PREFERENCE_USER_NAME";
    private static final String PREFERENCE_CARD_ID = "PREFERENCE_CARD_ID";
    private static final String PREFERENCE_REFRESH_TIMEOUT = "PREFERENCE_REFRESH_TIMEOUT";
    private static final String PREFERENCE_MAX_VIOLATION_IMAGES_COUNT = "PREFERENCE_MAX_VIOLATION_IMAGES_COUNT";
    private static final String PREFERENCE_MIN_VIOLATION_IMAGES_COUNT = "PREFERENCE_MIN_VIOLATION_IMAGES_COUNT";
    private static final String PREFERENCE_UVD_REGION_DEFAULT = "PREFERENCE_UVD_REGION_DEFAULT";
    private static final String PREFERENCE_CHECK_NEW_VERSION_TIME = "PREFERENCE_CHECK_NEW_VERSION_TIME";
    private static final String PREFERENCE_NEW_VERSION_NAME = "PREFERENCE_NEW_VERSION_NAME";

    private static final String PREFERENCE_TECHNICIAN_DEBUG_CARD_ID = "PREFERENCE_TECHNICIAN_DEBUG_CARD_ID";
    private static final String PREFERENCE_WRECKER_DEBUG_CARD_ID = "PREFERENCE_WRECKER_DEBUG_CARD_ID";

    private static final String PREFERENCE_GCM_REGISTRATION_ID = "PREFERENCE_GCM_REGISTRATION_ID";
    private static final String PREFERENCE_GCM_APP_VERSION = "PREFERENCE_GCM_APP_VERSION";

    public static String getToken(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString(PREFERENCE_TOKEN, "");
    }

    public static void setToken(Context context, String token)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFERENCE_TOKEN, token);
        editor.commit();
    }

    public static String getUserFIO(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString(PREFERENCE_USER_FIO, "");
    }

    public static void setUserFIO(Context context, String userFIO)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFERENCE_USER_FIO, userFIO);
        editor.commit();
    }

    public static String getUserCardID(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString(PREFERENCE_CARD_ID, "");
    }

    public static void setUserCardId(Context context, String cardId)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFERENCE_CARD_ID, cardId);
        editor.commit();
    }

    public static String getUserName(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString(PREFERENCE_USER_NAME, "");
    }

    public static void setUserName(Context context, String userName)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFERENCE_USER_NAME, userName);
        editor.commit();
    }

    public static void setRefreshTimeout(Context context, int refreshTimeout)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(PREFERENCE_REFRESH_TIMEOUT, refreshTimeout);
        editor.commit();
    }



    public static void setMaxViolationImagesCount(Context context, int maxCount)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(PREFERENCE_MAX_VIOLATION_IMAGES_COUNT, maxCount);
        editor.commit();
    }


    public static void setMinViolationImagesCount(Context context, int minCount)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(PREFERENCE_MIN_VIOLATION_IMAGES_COUNT, minCount);
        editor.commit();
    }

    public static Employee getLastAuthTechnician(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = GsonHelper.getInstance();
        String employeeStr = settings.getString(PREFERENCE_LAST_AUTH_TECHNICIAN, "");
        if (!TextUtils.isEmpty(employeeStr))
        {
            return gson.fromJson(employeeStr, Employee.class);
        }
        return null;
    }

    public static void setLastAuthTechnician(Context context, Employee lastAuthTechnician)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        Gson gson = GsonHelper.getInstance();
        editor.putString(PREFERENCE_LAST_AUTH_TECHNICIAN, gson.toJson(lastAuthTechnician));
        editor.commit();
    }


    public static void setCheckNewVersionTime(Context context, long time)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(PREFERENCE_CHECK_NEW_VERSION_TIME, time);
        editor.commit();
    }



    public static String getTechnicianDebugCardId(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString(PREFERENCE_TECHNICIAN_DEBUG_CARD_ID, "");
    }


}
