package com.inspector.managers.dao;

import android.database.SQLException;

import com.inspector.data.Violation;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 11/13/13.
 */
public class ViolationDAO extends BaseDaoImpl<Violation, Integer>
{
    public ViolationDAO(ConnectionSource connectionSource, Class<Violation> dataClass) throws java.sql.SQLException
    {
        super(connectionSource, dataClass);
    }

    public ArrayList<Violation> getAllViolations() throws java.sql.SQLException
    {
        return new ArrayList<Violation>(this.queryForAll());
    }

    public ArrayList<Violation> getViolationByIndex(int index) throws android.database.SQLException, java.sql.SQLException
    {
        QueryBuilder<Violation, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Violation.ID_VIOLATION_FIELD_NAME, index);
        PreparedQuery<Violation> preparedQuery = queryBuilder.prepare();
        ArrayList<Violation> violations = new ArrayList<Violation>(query(preparedQuery));
        return violations;
    }

    public ArrayList<Violation> getViolationByStatus(String status) throws android.database.SQLException, java.sql.SQLException
    {
        QueryBuilder<Violation, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Violation.STATUS_VIOLATION_FIELD_NAME, status);
        PreparedQuery<Violation> preparedQuery = queryBuilder.prepare();
        ArrayList<Violation> violations = new ArrayList<Violation>(query(preparedQuery));
        return violations;
    }
}
