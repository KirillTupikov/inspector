package com.inspector.managers;

import android.content.Context;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class ResourceManager
{

    public static String readRawFile(Context context, int res)
    {
        String data = null;
        InputStream is = null;
        ByteArrayOutputStream baos = null;

        try
        {
            is = context.getResources().openRawResource(res);
            baos = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int len;
            while (true)
            {
                len = is.read(b);
                if (len > 0)
                    baos.write(b, 0, len);
                else
                    break;
            }
            data = baos.toString("UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            Log.d("ResourceManager", e.toString());
        }
        catch (IOException e)
        {
            Log.d("ResourceManager", e.toString());
        }
        finally
        {
            try
            {
                if (is != null)
                {
                    is.close();
                }
                if (baos != null)
                {
                    baos.close();
                }
            }
            catch (Exception e)
            {
                Log.d("ResourceManager", e.toString());
            }
        }
        return data;
    }

}
