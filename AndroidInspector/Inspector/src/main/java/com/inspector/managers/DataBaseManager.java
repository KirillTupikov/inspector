package com.inspector.managers;

import android.content.Context;

import com.inspector.data.Article;
import com.inspector.data.Car;
import com.inspector.data.ImageCar;
import com.inspector.data.Violation;
import com.inspector.helpers.DataBaseHelper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by kirill.tupikov on 7/11/13.
 */
public class DataBaseManager
{
    static private DataBaseManager instance;

    private DataBaseHelper dataBaseHelper;

    static public void init(Context context)
    {
        if (instance == null)
        {
            instance = new DataBaseManager(context);

            try
            {
                if (instance.getAllArticles().size() == 0)
                    instance.createTestArticle();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
    }

    public DataBaseManager(Context context)
    {
        dataBaseHelper = new DataBaseHelper(context);
    }

    static public DataBaseManager getInstance()
    {
        return instance;
    }

    public DataBaseHelper getDataBaseHelper()
    {
        return dataBaseHelper;
    }


    public void createTestArticle()
    {
        try
        {
            createArticle(new Article("11.8.1 часть 1", "11.8.1 часть 1 Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("11.9", "11.9. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("11.26", "11.26. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("11.29", "11.29. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.3 часть 1", "С12.3 часть 1. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.5 часть 2", "12.5 часть 2. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.7 часть 1", "12.7 часть 1. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.7 часть 2", "12.7 часть 2. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.8 часть 1", "12.8 часть 1. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.8 часть 3", "12.8 часть 3. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.8 часть 4", "12.8 часть 4. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.16 часть 4", "12.16 часть 4. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.16 часть 5", "12.16 часть 5. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.19 часть 3", "12.19 часть 3. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.19 часть 4", "12.19 часть 4. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.19 часть 6", "12.19 часть 6. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.21.1 часть 1", "12.21.1 часть 1. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.21.1 часть 2", "12.21.1 часть 2. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.21.1 часть 3", "12.21.1 часть 3. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.21.2 часть 1", "12.21.2 часть 1. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.26", "12.26. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("12.27", "12.27. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));
            createArticle(new Article("14.38", "14.38. Остановака или стоянка ТС на пешеходном переходе и ближе на 5 перед ним"));

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void updateListArticles(ArrayList<Article> newArticles) throws SQLException
    {
//        ArrayList<Article> currentArticles = getAllArticles();
//        boolean f;
//        //проверяем новые
//        for (Article newArticle : newArticles)
//        {
//            f = true;
//            for (Article currentArticle : currentArticles)
//            {
//                if (newArticle.getIdBak().equals(currentArticle.getIdBak()))
//                {
//                    currentArticle.setTitleArticle(newArticle.getCodeJson());
//                    currentArticle.setDescriptionArticle(newArticle.getTitleJson());
//                    createArticle(currentArticle);
//
//                    currentArticles.remove(currentArticle);
//                    f = false;
//
//                }
//
//                if (f)
//                {
//                    //если такогой статьи не было то создаем новую
//                    newArticle.setTitleArticle(newArticle.getCodeJson());
//                    newArticle.setDescriptionArticle(newArticle.getTitleJson());
//                    createArticle(newArticle);
//                }
//            }
//        }
//
//        //если в списке с существующими статьями что-то осталось, то это удаляем
//        if (currentArticles.size() != 0)
//        {
//            for (Article currentArticle : currentArticles)
//            {
//                deleteArticle(currentArticle);
//            }
//        }

        ArrayList<Article> currentArticles = getAllArticles();

        Iterator iteratorNew = newArticles.iterator();
        Iterator iteratorCurrent;

        boolean f;
        //проверяем новые
        while (iteratorNew.hasNext())
        {
            Article newArticle = (Article) iteratorNew.next();
            f = true;

            iteratorCurrent = currentArticles.iterator();
            while (iteratorCurrent.hasNext())
            {
                Article currentArticle = (Article) iteratorCurrent.next();

                if (newArticle.getIdBak() == currentArticle.getIdBak())
                {
                    currentArticle.setTitleArticle(newArticle.getCodeJson());
                    currentArticle.setDescriptionArticle(newArticle.getTitleJson());

                    createArticle(currentArticle);//обновляем в базе

                    currentArticles.remove(currentArticle);
                    f = false;
                }
            }

            if (f)
            {
                //если такогой статьи не было то создаем новую
                newArticle.setTitleArticle(newArticle.getCodeJson());
                newArticle.setDescriptionArticle(newArticle.getTitleJson());
                createArticle(newArticle);
            }
        }

        //если в списке с существующими статьями что-то осталось, то это удаляем
        if (currentArticles.size() != 0)
        {
            for (Article currentArticle : currentArticles)
            {
                deleteArticle(currentArticle);
            }
        }
    }

    public void createArticle(Article article) throws SQLException
    {
        dataBaseHelper.getArticleDAO().createOrUpdate(article);
    }

    public void deleteArticle(Article article) throws SQLException
    {
        dataBaseHelper.getArticleDAO().delete(article);
    }

    public ArrayList<Article> getAllArticles() throws java.sql.SQLException
    {
        return dataBaseHelper.getArticleDAO().getAllArticles();
    }

    public void createCar(Car car) throws SQLException
    {
        dataBaseHelper.getCarDAO().createOrUpdate(car);
    }

    public ArrayList<Car> getAllCars() throws java.sql.SQLException
    {
        return dataBaseHelper.getCarDAO().getAllCars();
    }

    public void createImageCar(ImageCar imageCar) throws SQLException
    {
        dataBaseHelper.getImageCarDAO().createOrUpdate(imageCar);
    }

    public ArrayList<ImageCar> getImagesCar() throws java.sql.SQLException
    {
        return dataBaseHelper.getImageCarDAO().getAllImagesCar();
    }

    public void createViolation(Violation violation) throws SQLException
    {
        dataBaseHelper.getViolationDAO().createOrUpdate(violation);
    }

    public void addViolationsHistory(ArrayList<Violation> violations) throws SQLException
    {
        ArrayList<Violation> currentViolations = getAllViolation();

        boolean f;

       for(Violation violation : violations)
       {
           f = true;
           for(Violation currentViolation : currentViolations)
           {
               if(currentViolation.getViolationID().equals(violation.getViolationID()))
               {
                   f = false;
                   break;
               }
           }

           if(f)
           {
               createViolation(violation);
           }
       }
    }

    public ArrayList<Violation> getAllViolation() throws java.sql.SQLException
    {
        return dataBaseHelper.getViolationDAO().getAllViolations();
    }

    public Violation getViolationByIndex(int index) throws java.sql.SQLException
    {
        return dataBaseHelper.getViolationDAO().getViolationByIndex(index).get(0);
    }

    public ArrayList<Violation> getViolationByStatus(String status) throws java.sql.SQLException
    {
        return dataBaseHelper.getViolationDAO().getViolationByStatus(status);
    }

    public void deleteViolation(Violation violation) throws SQLException
    {
        dataBaseHelper.getViolationDAO().delete(violation);
    }

}
