package com.inspector.managers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by kirill.tupikov on 10/1/13.
 */
public class ImageManager
{

    private static File directory;

    static private ImageManager instance;

    public static ImageManager getInstance()
    {
        if(instance == null)
            instance = new ImageManager();

        return instance;
    }

    public ImageManager()
    {
        createDirectory();
    }


    public Bitmap loadImageByPath(String path)
    {
        return BitmapFactory.decodeFile(getDirectory() + "/" + path);
    }

    public Bitmap resizeImage(Bitmap bitmap, int maxSize)
    {
        if (bitmap.getWidth() > maxSize ||bitmap.getHeight() >  maxSize)
        {
            int newWidth;
            int newHeight;

            if (bitmap.getWidth() > bitmap.getHeight())
            {
                newWidth = maxSize - 27;
                newHeight = (int) ((double) newWidth / (double) bitmap.getWidth() * bitmap.getHeight());
            }
            else
            {
                newHeight = maxSize - 27;
                newWidth = (int) ((double) newHeight / (double) bitmap.getHeight() * bitmap.getWidth());
            }
            return Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
        }
        else
            return bitmap;
    }

    public String saveImageFromUri(String uri, Context context)
    {
        try
        {
            URL url = new URL(uri);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return saveGenerateFileUri(myBitmap, context);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public String saveGenerateFileUri(Bitmap bitmap, Context context) throws IOException
    {
        OutputStream fOut = null;
        File file = new File(directory.getPath() + "/" + "photo_" + System.currentTimeMillis() + ".jpg");
        Log.d("Tag", "fileName = " + file);

        fOut = new FileOutputStream(file);

        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
        fOut.flush();
        fOut.close();

        MediaStore.Images.Media.insertImage(context.getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

        return file.getName();
    }

    public boolean deleteImage(String filePath)
    {
        File file = new File(directory.getPath() + "/" + filePath);
        if (file.exists())
        {
            file.delete();
            return true;
        }
        else
            return false;
    }

    private File createDirectory()
    {
        directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "ImageInspector");
        if (!directory.exists())
            directory.mkdirs();
        return directory;
    }

    public File getDirectory()
    {
        return directory;
    }


    public File saveGenerateFile(Bitmap bitmap, Context context) throws IOException
    {
        OutputStream fOut = null;
        File file = new File(directory.getPath() + "/" + "photo_" + System.currentTimeMillis() + ".jpg");
        Log.d("Tag", "fileName = " + file);

        fOut = new FileOutputStream(file);

        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
        fOut.flush();
        fOut.close();

        MediaStore.Images.Media.insertImage(context.getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

        return file;
    }
}
