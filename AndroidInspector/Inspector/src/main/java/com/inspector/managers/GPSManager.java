package com.inspector.managers;

import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.inspector.interfaces.IGPSCallback;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GPSManager
{
    private static final int gpsMinTime = 9000;
    private static final int gpsMinDistance = 0;

    private static LocationManager locationManager = null;
    private static LocationListener locationListener = null;
    private static IGPSCallback IGPSCallback = null;

    public GPSManager()
    {
        GPSManager.locationListener = new LocationListener()
        {
            public void onLocationChanged(final Location location)
            {
                if (GPSManager.IGPSCallback != null)
                {
                    GPSManager.IGPSCallback.onGPSUpdate(location);
                }
            }

            public void onProviderDisabled(final String provider)
            {
            }

            public void onProviderEnabled(final String provider)
            {
            }

            public void onStatusChanged(final String provider, final int status, final Bundle extras)
            {
            }
        };
    }

    public IGPSCallback getGPSCallback()
    {
        return GPSManager.IGPSCallback;
    }

    public void setGPSCallback(final IGPSCallback IGPSCallback)
    {
        GPSManager.IGPSCallback = IGPSCallback;
    }

    public void startListening(final Context context)
    {
        if (GPSManager.locationManager == null)
        {
            GPSManager.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        }

        final Criteria criteria = new Criteria();

        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setSpeedRequired(true);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);

        final String bestProvider = GPSManager.locationManager.getBestProvider(criteria, true);

        if (bestProvider != null && bestProvider.length() > 0)
        {
            GPSManager.locationManager.requestLocationUpdates(bestProvider, GPSManager.gpsMinTime,
                    GPSManager.gpsMinDistance, GPSManager.locationListener);
        }
        else
        {
            final List<String> providers = GPSManager.locationManager.getProviders(true);

            for (final String provider : providers)
            {
                GPSManager.locationManager.requestLocationUpdates(provider, GPSManager.gpsMinTime,
                        GPSManager.gpsMinDistance, GPSManager.locationListener);
            }
        }
    }

    public void stopListening()
    {
        try
        {
            if (GPSManager.locationManager != null && GPSManager.locationListener != null)
            {
                GPSManager.locationManager.removeUpdates(GPSManager.locationListener);
            }

            GPSManager.locationManager = null;
        }
        catch (final Exception ex)
        {

        }
    }

    static public String getAddress(Context context, Location location) throws IOException
    {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        Address address;
        String result = null;
        List<Address> list = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        address = list.get(0);
        result = address.getAddressLine(0) + ", " + address.getLocality();
        return result;
    }
    static public String getAddress(Context context, double latitude, double longitude) throws IOException
    {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        Address address;
        String result = null;
        List<Address> list = geocoder.getFromLocation(latitude, longitude, 1);
        address = list.get(0);
        result = address.getAddressLine(0) + ", " + address.getLocality();
        return result;
    }
}
