package com.inspector.managers.dao;

import com.inspector.data.ImageCar;
import com.inspector.data.Violation;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 12/3/13.
 */
public class ImageCarDAO extends BaseDaoImpl<ImageCar, Integer>
{
    public ImageCarDAO(ConnectionSource connectionSource, Class<ImageCar> dataClass) throws java.sql.SQLException
    {
        super(connectionSource, dataClass);
    }

    public ArrayList<ImageCar> getAllImagesCar() throws java.sql.SQLException
    {
        return new ArrayList<ImageCar>(this.queryForAll());
    }
}

