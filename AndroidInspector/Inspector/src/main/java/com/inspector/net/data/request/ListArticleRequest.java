package com.inspector.net.data.request;

/**
 * Created by GSTU_Robotics on 03.12.13.
 */
public class ListArticleRequest
{
    public static final String TOKEN = "token";

    private String token;

    public ListArticleRequest()
    {
    }

    public ListArticleRequest(String token)
    {
        this.token = token;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String toString()
    {
        return TOKEN + "=" + token;
    }
}
