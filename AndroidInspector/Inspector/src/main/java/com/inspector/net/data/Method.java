package com.inspector.net.data;

/**
 * Created by kirill.tupikov on 11/29/13.
 */
public enum Method
{
    GET, POST;
}
