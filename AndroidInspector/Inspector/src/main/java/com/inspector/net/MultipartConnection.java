package com.inspector.net;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import com.inspector.managers.ImageManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.assist.ViewScaleType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.decode.ImageDecodingInfo;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import javax.net.ssl.*;

import java.io.*;
import java.net.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: alexander.kozlov
 * Date: 5/20/13
 * Time: 12:31 PM
 */
public class MultipartConnection
{
    public static final String UPLOAD_PROGRESS_BROADCAST = "UPLOAD_PROGRESS_BROADCAST";
    private final String BOUNDARY = "XXXEDCBOUNDARYXXX";
    private String requestString;
    Context context;
    boolean isPublishProgress;

    public MultipartConnection(Context context, String url)
    {
        this.context = context;
        requestString = url;
    }

    public String uploadImage(String fileParameterName, String fileUrl, String fileName, Map<String, String> parameters)
    {
        return uploadData(fileParameterName, fileUrl, fileName, parameters, true);
    }

    public String uploadFile(String fileParameterName, String fileUrl, String fileName, Map<String, String> parameters)
    {
        return uploadData(fileParameterName, fileUrl, fileName, parameters, false);
    }


    private int nextProgressValue = 5;

    private boolean publishProgress(int progress, int maxSize, String url)
    {
        //post every 5%
        if (isPublishProgress)
        {
            int percentProgress = (progress * 100) / maxSize;
            if (percentProgress >= nextProgressValue)
            {
                Intent intent = new Intent(UPLOAD_PROGRESS_BROADCAST);
                intent.putExtra("EXTRA_PROGRESS", percentProgress);
                intent.putExtra("EXTRA_URL", url);
                context.sendBroadcast(intent);

                nextProgressValue = 5 * (percentProgress / 5) + 5;
                return true;
            }
        }
        return false;
    }

    private String uploadData(String fileParameterName, String fileUrl, String fileName, Map<String, String> parameters, boolean isImage)
    {

        String resultString = new String("");
        StringBuffer requestBody = new StringBuffer();

        try
        {

            URLConnection connection = null;
            URL url = new URL(requestString);

            connection = url.openConnection();

            /////////////////////////////

            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            httpConnection.setRequestMethod("POST");

            httpConnection.setUseCaches(false);
            httpConnection.setDoOutput(true);
            httpConnection.setDoInput(true);

            httpConnection.setRequestProperty("Accept", "*/*");
            httpConnection.setRequestProperty("User-Agent", "MyAndroid/1.6");
            httpConnection.setRequestProperty("Accept-Charset", "UTF-8");
            //httpConnection.setRequestProperty("Content-Language", "ru-RU");
            httpConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
            httpConnection.setRequestProperty("Connection", "Keep-Alive");
            if (url.getProtocol().equals("https"))
            {
                HttpsURLConnection httpsConnection = (HttpsURLConnection) connection;
                httpsConnection.setSSLSocketFactory(getAllHostsValidSocketFactory());
                httpsConnection.setHostnameVerifier(getAllHostsValidVerifier());
            }

            // собрать буфер для отправки запроса
            String contentDisposition = "Content-Disposition: form-data; name=\"" + fileParameterName + "\"; filename=\"" + fileName + "\"";
            //String contentType = "Content-Type: application/octet-stream\nContent-Transfer-Encoding: binary";
            String contentType = "Content-Type: image/jpeg\nContent-Transfer-Encoding: binary";

            //add parameters to request
            for (Map.Entry<String, String> parameterEntry : parameters.entrySet())
            {
                addParameter(requestBody, parameterEntry.getKey(), parameterEntry.getValue());
            }

            requestBody.append("--");
            requestBody.append(BOUNDARY);
            requestBody.append("\r\n");

            //add file
            requestBody.append(contentDisposition);
            requestBody.append("\r\n");
            requestBody.append(contentType);
            requestBody.append("\r\n");
            requestBody.append("\r\n");

            int bytesAvailable, bufferSize, bytesRead, endBlockSize;
            byte[] buffer;
            int maxBufferSize = 7 * 1024 * 1024;
            //FileInputStream fileInputStream = new FileInputStream(file);
            InputStream inputStream;
//            if (isImage)
//            {
//                inputStream = getBitmap(fileUrl);
//            }
//            else
//            {
//                inputStream = getFile(fileUrl);
//            }

            inputStream = getBitmap(fileUrl);
            bytesAvailable = inputStream.available();

            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[1024];


            // установка Content-Length

            endBlockSize = BOUNDARY.length() + 8;

            httpConnection.setRequestProperty("Content-Length",
                    String.valueOf(requestBody.toString().length() +
                            bufferSize +
                            endBlockSize
                    ));


            // соединение
            httpConnection.connect();

            // отправка первой части запроса
            DataOutputStream dataOS = new DataOutputStream(httpConnection.getOutputStream());
            dataOS.writeBytes(requestBody.toString());

            // отправка файла
            bytesRead = 0;
            int lenSum = 0;
            while ((bytesRead = inputStream.read(buffer)) != -1)
            {
                dataOS.write(buffer, 0, bytesRead);
                lenSum += bytesRead;
                if (publishProgress(lenSum, bufferSize, fileUrl))
                {
                    dataOS.flush();
                }
                ;
            }

            //bytesRead = inputStream.read(buffer, 0, bufferSize);
            //dataOS.write(buffer, 0, bytesRead);

            //////////////////////////////////////////////

            dataOS.writeBytes("\r\n");
            dataOS.writeBytes("--");
            dataOS.writeBytes(BOUNDARY);
            dataOS.writeBytes("--");
            dataOS.writeBytes("\r\n");

            inputStream.close();

            dataOS.flush();
            dataOS.close();

            int responseCode = httpConnection.getResponseCode();
            if (responseCode == 200)
            {
                // если все прошло нормально, получаем результат
                // может быть другой код, см. http://developer.android.com/reference/java/net/HttpURLConnection.html


                InputStream in = httpConnection.getInputStream();

                InputStreamReader isr = new InputStreamReader(in, "UTF-8");

                StringBuffer data = new StringBuffer();
                int c;
                while ((c = isr.read()) != -1)
                {
                    data.append((char) c);
                }


                resultString = new String(data.toString());

            }
            else
            {
                resultString = "сервер не ответил";
            }


        }
        catch (MalformedURLException e)
        {

            resultString = "MalformedURLException:" + e.getMessage();
        }
        catch (IOException e)
        {

            resultString = "IOException:" + e.getMessage();
        }
        catch (Exception e)
        {

            resultString = "Exception:" + e.getMessage();
        }

        requestBody = null;
        System.gc();

        return resultString;
    }

    private InputStream getBitmap(String imageUrl) throws IOException
    {
//        BaseImageDownloader imageDownloader = new BaseImageDownloader(context);
//        DisplayImageOptions displayImageOptions = DisplayImageOptions.createSimple();
//        BaseImageDecoder decoder = new BaseImageDecoder(false);
//        ImageDecodingInfo decodingInfo = new ImageDecodingInfo(null, imageUrl, new ImageSize(800, 800), ViewScaleType.FIT_INSIDE, imageDownloader, displayImageOptions);
//        Bitmap bitmap = decoder.decode(decodingInfo);
        Bitmap bitmap = ImageManager.getInstance().loadImageByPath(imageUrl);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }

//    private InputStream getFile(String fileUrl) throws IOException
//    {
//        BaseImageDownloader imageDownloader = new BaseImageDownloader(context);
//        return imageDownloader.getStream(fileUrl, null);
//    }

    private void addParameter(StringBuffer requestBody, String parameterName, String parameterValue)
    {
        requestBody.append("--");
        requestBody.append(BOUNDARY);
        requestBody.append("\r\n");
        requestBody.append("Content-Disposition: form-data; name=\"" + parameterName + "\"");
        requestBody.append("\r\n");
        requestBody.append("\r\n");
        requestBody.append(parameterValue + "\r\n");
    }

    public boolean isPublishProgress()
    {
        return isPublishProgress;
    }

    public void setPublishProgress(boolean publishProgress)
    {
        isPublishProgress = publishProgress;
    }

    private static SSLSocketFactory sAllHostsValidSocketFactory;

    private static SSLSocketFactory getAllHostsValidSocketFactory()
            throws NoSuchAlgorithmException, KeyManagementException
    {
        if (sAllHostsValidSocketFactory == null)
        {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager()
                    {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers()
                        {
                            return null;
                        }

                        public void checkClientTrusted(X509Certificate[] certs, String authType)
                        {
                        }

                        public void checkServerTrusted(X509Certificate[] certs, String authType)
                        {
                        }
                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            sAllHostsValidSocketFactory = sc.getSocketFactory();
        }

        return sAllHostsValidSocketFactory;
    }

    private static HostnameVerifier sAllHostsValidVerifier;

    private static HostnameVerifier getAllHostsValidVerifier()
    {
        if (sAllHostsValidVerifier == null)
        {
            sAllHostsValidVerifier = new HostnameVerifier()
            {
                public boolean verify(String hostname, SSLSession session)
                {
                    return true;
                }
            };
        }

        return sAllHostsValidVerifier;
    }
}
