package com.inspector.net.data.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kirill.tupikov on 12/3/13.
 */
public class RemotePathImageAttachResponse extends GenericResponse
{
    public final static String REMOTE_PATH = "data";

    @SerializedName(REMOTE_PATH)
    private String remotePath;

    public String getRemotePath()
    {
        return remotePath;
    }

    public void setRemotePath(String remotePath)
    {
        this.remotePath = remotePath;
    }
}
