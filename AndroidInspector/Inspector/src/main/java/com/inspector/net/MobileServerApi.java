package com.inspector.net;

import android.content.Context;
import android.content.res.Configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inspector.Setting;
import com.inspector.ThisApplication;
import com.inspector.data.ImageCar;
import com.inspector.data.Violation;
import com.inspector.exception.MobileServerAPIException;
import com.inspector.managers.DataBaseManager;
import com.inspector.managers.SessionManager;
import com.inspector.net.data.Method;
import com.inspector.net.data.request.GetHistoryRequest;
import com.inspector.net.data.request.InspectorLoginRequest;
import com.inspector.net.data.request.ListArticleRequest;
import com.inspector.net.data.request.LogoutRequest;
import com.inspector.net.data.request.ViolationRequest;
import com.inspector.net.data.response.GenericResponse;
import com.inspector.net.data.response.GetHistoryResponse;
import com.inspector.net.data.response.InspectorLoginResponse;
import com.inspector.net.data.response.ListArticleResponse;
import com.inspector.net.data.response.LogoutResponse;
import com.inspector.net.data.response.RemotePathImageAttachResponse;
import com.inspector.net.data.response.ViolationResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.ksoap2.HeaderProperty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Created with IntelliJ IDEA.
 * User: Kirill
 * Date: 06.03.13
 * Time: 16:13
 * To change this template use File | Settings | File Templates.
 */
public class MobileServerApi
{

    private Gson JSON_CONVERTER;

    public MobileServerApi()
    {
        GsonBuilder gsonBuilder = new GsonBuilder();
        //gsonBuilder.excludeFieldsWithoutExposeAnnotation();
        JSON_CONVERTER = gsonBuilder.create();

        //JSON_CONVERTER = new Gson();
    }

    public InspectorLoginResponse authenticateRequest(InspectorLoginRequest request) throws MobileServerAPIException, Exception
    {
        return executeApiCall(Setting.MOBILE_SERVER_API_URL_KEY_AUTHENTICATE, request, InspectorLoginResponse.class, Method.GET, null);
    }

    public ViolationResponse createViolationRequest(ViolationRequest request) throws MobileServerAPIException, Exception
    {
        return executeApiCall(Setting.MOBILE_SERVER_API_URL_KEY_CREATE_VIOLATION, request, ViolationResponse.class, Method.POST, null);
    }

    public Violation violationPhotoAttachRequest(Context context, Violation violation) throws MobileServerAPIException, Exception
    {
        MultipartConnection multipartConnection = new MultipartConnection(context, Setting.MOBILE_SERVER_API_URL_PHOTO_ATTACH);
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("violationId", violation.getViolationID());
        parameters.put(SessionManager.PARAM_TOKEN, SessionManager.getInstance().getToken());

        String total = null;

        for (ImageCar imageCar : violation.getCar().getImageCars())
        {
            total = multipartConnection.uploadImage("photo", imageCar.getLocalPathImage(), "violationGeneralPhoto.jpg", parameters);

            RemotePathImageAttachResponse genericResponse = JSON_CONVERTER.fromJson(total, RemotePathImageAttachResponse.class);

            if (!genericResponse.isSuccess())
            {
                throw JSON_CONVERTER.fromJson(total, MobileServerAPIException.class);
            }

            imageCar.setRemotePathImage(genericResponse.getRemotePath());

            DataBaseManager.getInstance().createImageCar(imageCar);//обновляем пути изображений
        }

        return violation;
    }

    public ListArticleResponse getListArticlesViolation(ListArticleRequest request) throws MobileServerAPIException, Exception
    {
        return executeApiCall(Setting.MOBILE_SERVER_API_URL_LIST_VIOLATION_ARTICLE, request, ListArticleResponse.class, Method.GET, null);
    }

    public LogoutResponse logout(LogoutRequest request) throws MobileServerAPIException, Exception
    {
        return executeApiCall(Setting.MOBILE_SERVER_API_URL_KEY_LOGOUT, request, LogoutResponse.class, Method.GET, null);
    }

    public GetHistoryResponse getHistory(GetHistoryRequest request) throws MobileServerAPIException, Exception
    {
        return executeApiCall(Setting.MOBILE_SERVER_API_GET_HISTORY, request, GetHistoryResponse.class, Method.GET, null);
    }


    //private <T extends GenericResponse> T executeApiCall(String url_request, Object request, Class<T> resultClass) throws MobileServerAPIException, Exception
    private <T extends GenericResponse> T executeApiCall(String url_request, Object request, Class<T> tClass, Method method, ArrayList<String> uploadFilePath) throws MobileServerAPIException, Exception
    {
        String url = new StringBuilder(url_request).append("?").
                append(request.toString()).toString();

        HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response = null;
        switch (method)
        {
            case GET:
                HttpGet httpGetRequest = new HttpGet(url);

                HttpParams params = httpclient.getParams();
                HttpConnectionParams.setConnectionTimeout(params, Setting.OPERATION_TIMEOUT);
                HttpConnectionParams.setSoTimeout(params, Setting.OPERATION_TIMEOUT);

                httpGetRequest.addHeader("Accept", "*/*");
                httpGetRequest.addHeader("Accept-Charset", "UTF-8");
                httpGetRequest.addHeader("Accept-Encoding", "gzip");

                response = httpclient.execute(httpGetRequest);

                break;

            case POST:

                HttpPost httpPostRequest = new HttpPost(url);

                httpPostRequest.addHeader("Accept", "*/*");
                httpPostRequest.addHeader("Accept-Charset", "UTF-8");
                httpPostRequest.addHeader("Accept-Encoding", "gzip");

                response = httpclient.execute(httpPostRequest);

                break;

            default:
                return null;
        }


        BufferedReader r = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null)
        {
            total.append(line);
        }
        T genericResponse = JSON_CONVERTER.fromJson(total.toString(), tClass);
        if (!genericResponse.isSuccess())
        {
            //throw new Exception("result isn't succeeded: " + total);
            throw JSON_CONVERTER.fromJson(total.toString(), MobileServerAPIException.class);
            //throw new MobileServerAPIException(String.valueOf(genericResponse.isSuccess()), String.valueOf(total));
        }
        //String normalizedString = normalizeJson(genericResponse.getResponse().toString());
        return genericResponse;
    }


    private List<HeaderProperty> getHeaderProperties()
    {
        List<HeaderProperty> headers = new ArrayList<HeaderProperty>();
        Configuration config = ThisApplication.getInstance().getResources().getConfiguration();
        if (config.locale.getLanguage().equals("ru"))
        {
            headers.add(new HeaderProperty("Accept-Language", "ru"));
        }
        else
        {
            headers.add(new HeaderProperty("Accept-Language", "en-us"));
        }

        headers.add(new HeaderProperty("Accept", "*/*"));
        headers.add(new HeaderProperty("Accept-Charset", "UTF-8"));
        headers.add(new HeaderProperty("Accept-Encoding", "gzip"));
        return headers;
    }


    private static String convertStreamToString(InputStream is, boolean isGzipEnabled)
            throws IOException
    {
        InputStream cleanedIs = is;
        if (isGzipEnabled)
        {
            cleanedIs = new GZIPInputStream(is);
        }

        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(new InputStreamReader(cleanedIs, "UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (String line; (line = reader.readLine()) != null; )
            {
                sb.append(line);
                sb.append("\n");
            }

            return sb.toString();
        }
        finally
        {
            if (reader != null)
            {
                reader.close();
            }

            cleanedIs.close();

            if (isGzipEnabled)
            {
                is.close();
            }
        }
    }

}
