package com.inspector.net.data.request;

import com.google.gson.annotations.Expose;

/**
 * Created by GSTU_Robotics on 28.11.13.
 */
public class LogoutRequest
{
    public static final String TOKEN = "token";

    //@Expose
    protected String token;

    public LogoutRequest()
    {
    }

    public LogoutRequest(String token)
    {
        this.token = token;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String toString()
    {
        return TOKEN + "=" + token;
    }
}
