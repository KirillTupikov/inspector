package com.inspector.net.data.response;

/**
 * Created by kirill.tupikov on 11/25/13.
 */
public class ViolationResponse extends GenericResponse
{
    private String data;

    public String getData()
    {
        return data;
    }

    public void setData(String data)
    {
        this.data = data;
    }
}
