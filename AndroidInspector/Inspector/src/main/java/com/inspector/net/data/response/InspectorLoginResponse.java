package com.inspector.net.data.response;


import com.google.gson.annotations.Expose;
import com.inspector.data.User;

import java.io.Serializable;

/**
 * Created by kirill.tupikov on 11/25/13.
 */
public class InspectorLoginResponse extends GenericResponse
{
    private User data;

    public User getData()
    {
        return data;
    }

    public void setData(User data)
    {
        this.data = data;
    }
}
