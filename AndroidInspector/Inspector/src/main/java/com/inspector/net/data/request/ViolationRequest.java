package com.inspector.net.data.request;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.inspector.data.Violation;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by kirill.tupikov on 11/25/13.
 */
@DatabaseTable
public class ViolationRequest
{

    public static final String TOKEN = "token";
    public static final String VIOLATION_STRING = "violationString";

    private Gson JSON_CONVERTER;

    private String token;
    private Violation violation;

    public ViolationRequest()
    {
    }

    public ViolationRequest(String token, String policemanName, Violation violation)
    {
        this.token = token;
        this.violation = violation;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public Violation getViolation()
    {
        return violation;
    }

    public void setViolation(Violation violation)
    {
        this.violation = violation;
    }

    public String toString()
    {
        //http://192.168.11.59:8080/remote/technician/violation/create?token=5dlfpt71q9vr3h86a2p7&
        // violationString={"address":"adrress","code":"11.8.1 часть 1","tcNumber":"5ОРХ","protocolNumber":"56 АР 5677457",
        // "uvdRegion":{"id":6},"title":"ПДД","longitude":31.01172104,"latitude":52.44043796}&

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.excludeFieldsWithoutExposeAnnotation();
        JSON_CONVERTER = gsonBuilder.create();

//      append(URLEncoder.encode(JSON_CONVERTER.toJson(request), "UTF-8")).toString();
        String str = null;
        try
        {
            str = TOKEN + "=" + token + "&" +
                    VIOLATION_STRING + "=" + URLEncoder.encode(JSON_CONVERTER.toJson(violation), "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        return str;
    }
}
