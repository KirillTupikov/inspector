package com.inspector.net.data.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by kirill.tupikov on 11/25/13.
 */
public class InspectorLoginRequest
{
    private static final String UID = "uid";

    //@Expose
    protected String uid;

    public InspectorLoginRequest()
    {
    }

    public InspectorLoginRequest(String uid)
    {
        this.uid = uid;
    }

    public String getUid()
    {
        return uid;
    }

    public void setUid(String uid)
    {
        this.uid = uid;
    }

    public String toString()
    {
        return UID + "=" + uid;
    }
}
