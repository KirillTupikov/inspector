package com.inspector.net.data.response;

/**
 * Created with IntelliJ IDEA.
 * User: Kirill
 * Date: 06.03.13
 * Time: 20:14
 * To change this template use File | Settings | File Templates.
 */
public class GenericResponse
{

    protected boolean success;

    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

}
