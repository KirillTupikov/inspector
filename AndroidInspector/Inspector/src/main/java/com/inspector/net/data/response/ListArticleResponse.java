package com.inspector.net.data.response;

import com.inspector.data.Article;

import java.util.ArrayList;

/**
 * Created by GSTU_Robotics on 03.12.13.
 */
public class ListArticleResponse extends GenericResponse
{
    private ArrayList<Article> data;

    public ArrayList<Article> getData()
    {
        return data;
    }

    public void setData(ArrayList<Article> data)
    {
        this.data = data;
    }
}
