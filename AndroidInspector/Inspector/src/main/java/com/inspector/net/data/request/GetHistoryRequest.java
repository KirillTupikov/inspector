package com.inspector.net.data.request;

/**
 * Created by GSTU_Robotics on 17.12.13.
 */
public class GetHistoryRequest
{
    private String token;

    private String policemanName;

    public GetHistoryRequest()
    {
    }

    public GetHistoryRequest(String token, String policemanName)
    {
        this.token = token;
        this.policemanName = policemanName;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getPolicemanName()
    {
        return policemanName;
    }

    public void setPolicemanName(String policemanName)
    {
        this.policemanName = policemanName;
    }

    public String toString()
    {
        return "token=" + token + "&" +
                "policemanName=" + policemanName;
    }
}
