package com.inspector.net.data.response;

import com.inspector.data.Violation;

import java.util.ArrayList;

/**
 * Created by GSTU_Robotics on 17.12.13.
 */
public class GetHistoryResponse extends GenericResponse
{
    private ArrayList<Violation> data;

    public ArrayList<Violation> getData()
    {
        return data;
    }

    public void setData(ArrayList<Violation> data)
    {
        this.data = data;
    }
}
