package com.inspector.gui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inspector.R;
import com.inspector.data.Violation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Helm on 18.11.13.
 */
public class HistoryListAdapter extends BaseAdapter
{
    private ArrayList<Violation> contentItems;
    private LayoutInflater layoutInflater;
    private Context context;

    private static class ViewHolder
    {
        TextView titleDate;
        TextView titleArticle;
        TextView titleNumber;
        ImageView statusViolation;
    }

    public HistoryListAdapter(Context context, ArrayList<Violation> contentItems)
    {
        this.context = context;
        this.contentItems = contentItems;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        return contentItems.size();
    }

    @Override
    public Object getItem(int i)
    {
        return contentItems.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return (long) i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        ViewHolder viewHolder;

        if (view == null)
        {
            view = layoutInflater.inflate(R.layout.item_history, null);
            viewHolder = new ViewHolder();

            viewHolder.titleDate = (TextView) view.findViewById(R.id.labelDateViolation);
            viewHolder.titleArticle = (TextView) view.findViewById(R.id.labelArticle);
            viewHolder.titleNumber = (TextView) view.findViewById(R.id.labelNumber);
            viewHolder.statusViolation = (ImageView) view.findViewById(R.id.statusViolation);

            view.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) view.getTag();
        }

        final Violation violation = (Violation) getItem(i);

        SimpleDateFormat simpleDateFormatDate = new SimpleDateFormat("MM-dd-yyyy");
        SimpleDateFormat simpleDateFormatTime = new SimpleDateFormat("HH:mm");
        String date = simpleDateFormatDate.format(violation.getDate());
        String time = simpleDateFormatTime.format(violation.getDate());
        viewHolder.titleDate.setText(date + "\n" + time);

        viewHolder.titleArticle.setText(violation.getArticle().getTitleArticle());
        viewHolder.titleNumber.setText(violation.getCar().getNumberCar());

        if(violation.getStatus().equals(Violation.VIOLATION_COMPLETE))
            viewHolder.statusViolation.setImageResource(R.drawable.ic_action_accept);
        else
            viewHolder.statusViolation.setImageResource(R.drawable.ic_action_empty);

        return view;
    }
}
