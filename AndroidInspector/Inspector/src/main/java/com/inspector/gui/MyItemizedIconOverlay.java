package com.inspector.gui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.view.MotionEvent;

import com.inspector.R;

import org.osmdroid.ResourceProxy;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GSTU_Robotics on 12.12.13.
 */
public class MyItemizedIconOverlay extends ItemizedIconOverlay<OverlayItem>
{
    private ArrayList<OverlayItem> overlayItemArray;
    private Context context;

    public MyItemizedIconOverlay(Context context,
                                 ArrayList<OverlayItem> overlayItemArray,
                                 List<OverlayItem> pList, org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener<OverlayItem> pOnItemGestureListener,
                                 ResourceProxy pResourceProxy)
    {
        super(pList, pOnItemGestureListener, pResourceProxy);
        this.overlayItemArray = new ArrayList<OverlayItem>(overlayItemArray);
        this.context = context;

        // TODO Auto-generated constructor stub
    }

    @Override
    public void draw(Canvas canvas, MapView mapview, boolean arg2)
    {
        // TODO Auto-generated method stub
        super.draw(canvas, mapview, arg2);

        if (!overlayItemArray.isEmpty())
        {

            //overlayItemArray have only ONE element only, so I hard code to get(0)
            GeoPoint in = overlayItemArray.get(0).getPoint();

            Point out = new Point();
            mapview.getProjection().toPixels(in, out);

            Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_menu_mylocation);
            canvas.drawBitmap(bm,
                    out.x - bm.getWidth() / 2,    //shift the bitmap center
                    out.y - bm.getHeight() / 2,    //shift the bitmap center
                    null);
        }
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event, MapView mapView)
    {
        // TODO Auto-generated method stub
        //return super.onSingleTapUp(event, mapView);
        return true;
    }
}
