package com.inspector.gui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.android.gms.internal.ar;
import com.inspector.R;
import com.inspector.data.Article;

import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 7/8/13.
 */
public class ArticleListAdapter extends BaseAdapter
{
    private int checkedItemId = -1;

    private ArrayList<Article> contentItems;
    private LayoutInflater layoutInflater;
    private Context context;

    private static class ViewHolder
    {
        RadioButton checkArticle;
        TextView titleArticle;
    }

    public ArticleListAdapter(Context context, ArrayList<Article> contentItems)
    {
        this.context = context;
        this.contentItems = contentItems;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        return contentItems.size();
    }

    @Override
    public Object getItem(int i)
    {
        return contentItems.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return (long) i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        ViewHolder viewHolder;

        if (view == null)
        {
            view = layoutInflater.inflate(R.layout.item_article, null);
            viewHolder = new ViewHolder();

            viewHolder.checkArticle = (RadioButton) view.findViewById(R.id.checkArticle);
            viewHolder.titleArticle = (TextView) view.findViewById(R.id.titleArticle);

            view.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) view.getTag();
        }

        final Article article = (Article) getItem(i);

        viewHolder.titleArticle.setText(article.getTitleArticle());

        if (checkedItemId == -1)
        {
            checkedItemId = article.getId();
        }

        if (article.getId() == checkedItemId)
        {
            if (!viewHolder.checkArticle.isChecked())
            {
                viewHolder.checkArticle.setChecked(true);
            }
        }
        else
        {
            if (viewHolder.checkArticle.isChecked())
            {
                viewHolder.checkArticle.setChecked(false);
            }
        }

//        viewHolder.checkArticle.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View view)
//            {
//                checkedItemId = article.getId();
//                notifyDataSetChanged();
//            }
//        });

        return view;
    }

    public int getCheckedItemId()
    {
        return checkedItemId;
    }

    public void setCheckedItemId(int checkedItemId)
    {
        this.checkedItemId = checkedItemId;
    }
}
