package com.inspector.gui;

import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import java.util.HashMap;

/**
 * Created by alexander.kozlov on 10/17/13.
 */
public class VehicleNumberKeyboardListener implements KeyboardView.OnKeyboardActionListener, View.OnKeyListener
{

    EditText editText;
    private KeyboardView keyboardView;
    private Keyboard ruKeyboard;
    private Keyboard enKeyboard;

    public VehicleNumberKeyboardListener(EditText editText, KeyboardView keyboardView, Keyboard ruKeyboard, Keyboard enKeyboard)
    {
        this.editText = editText;
        this.keyboardView = keyboardView;
        this.ruKeyboard = ruKeyboard;
        this.enKeyboard = enKeyboard;
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event)
    {
        return true;
    }

    @Override
    public void onPress(int primaryCode)
    {
    }

    @Override
    public void onRelease(int primaryCode)
    {
    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes)
    {
        HashMap<String, String> keyCodeMap = new HashMap<String, String>();
        keyCodeMap.put("1", "1");
        keyCodeMap.put("2", "2");
        keyCodeMap.put("3", "3");
        keyCodeMap.put("4", "4");
        keyCodeMap.put("5", "5");
        keyCodeMap.put("6", "6");
        keyCodeMap.put("7", "7");
        keyCodeMap.put("8", "8");
        keyCodeMap.put("9", "9");
        keyCodeMap.put("0", "0");
        keyCodeMap.put("1040", "А");
        keyCodeMap.put("1042", "В");
        keyCodeMap.put("1043", "Е");
        keyCodeMap.put("1044", "К");
        keyCodeMap.put("1045", "М");
        keyCodeMap.put("1046", "Н");
        keyCodeMap.put("1047", "О");
        keyCodeMap.put("1048", "Р");
        keyCodeMap.put("1049", "С");
        keyCodeMap.put("1050", "Т");
        keyCodeMap.put("1051", "У");
        keyCodeMap.put("1052", "Х");
        keyCodeMap.put("1053", "C");
        keyCodeMap.put("1054", "D");

        String c = keyCodeMap.get(String.valueOf(primaryCode));
        if (!(c == null))
        {
            editText.append(c);
        }
        else
        {
            switch (primaryCode)
            {
                case -5:
                    if (editText.getText().toString().length() > 0)
                        editText.setText(editText.getText().toString().substring(0, editText.getText().toString().length() - 1));
                    break;
                case -4:
                    keyboardView.setKeyboard(ruKeyboard);
                    break;
                case -3:
                    keyboardView.setKeyboard(enKeyboard);
                    break;
            }
        }
    }

    @Override
    public void onText(CharSequence text)
    {
    }

    @Override
    public void swipeLeft()
    {
    }

    @Override
    public void swipeRight()
    {
    }

    @Override
    public void swipeDown()
    {
    }

    @Override
    public void swipeUp()
    {
    }
}
