package com.inspector;

import android.content.SharedPreferences;
import android.view.ViewConfiguration;

import java.lang.reflect.Field;

public class ThisApplication extends android.app.Application
{
    private static ThisApplication instance = null;

    public static final String PROPERTIES_FILE_NAME = "properties";

    private int maximumSizeImage = 0;

    @Override
    public void onCreate()
    {
        super.onCreate();

        getOverflowMenu();

        instance = this;
    }

    public static ThisApplication getInstance()
    {
        return instance;
    }


    private void getOverflowMenu()
    {

        try
        {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null)
            {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static SharedPreferences getPreferences()
    {
        return instance.getSharedPreferences(PROPERTIES_FILE_NAME, 0);
    }

    public int getMaximumSizeImage()
    {
        return maximumSizeImage;
    }

    public void setMaximumSizeImage(int maximumSizeImage)
    {
        this.maximumSizeImage = maximumSizeImage;
    }
}
